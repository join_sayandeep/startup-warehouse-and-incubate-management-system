<?php

namespace App\Http\Middleware;

use Closure;

class OpStatusCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->session()->has('opsession')) {   
               
            return $next($request);
        }
        else{
            return redirect('/op/login');
        }
    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class IncubStatusCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->session()->has('incubateesession')) {   
               
            return $next($request);
        }
        else{
            return redirect('/');
        }
    }
}

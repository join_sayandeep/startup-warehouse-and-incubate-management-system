<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Users;
use App\Operators;
use App\Incubatee;
use App\Attendence;

class OperatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function loginFormShow()
    {
        return view('pages.op.login');
    }

    
    public function loginCheck(Request $request)
    {
        $company_email = $request->input('username');
        $password = $request->input('password');

        $users = Users::where('email',$company_email)->first();

        if(!empty($users) && $users['status'] == 'op')
        {
                if($users['account_status'] != 0){
                    $incub_pass_reset_check = Operators::where('email',$company_email)->first();
                            if($incub_pass_reset_check['password_status'] != 1){                                                                                          
                                if(Hash::check($password,$users['password'])){
                                         return view('pages.op.resetPassword',['cmp_email' => $company_email]);
                                }else{
                                            session()->flash('message.level', 'danger');
                                            session()->flash('message.content', 'Wrong Username or Password');
                        
                                            return redirect('/op/login');
                                 }
                            }else{
                                            if(Hash::check($password,$users['password'])){
                                                    
                                                session(['opsession' => $company_email,'name' => $users['company_name'],'profile_img' => $incub_pass_reset_check['img']]);
                                                return redirect('/op/attendence/show');
                                            }else{
                                                session()->flash('message.level', 'danger');
                                                session()->flash('message.content', 'Wrong Username or Password');
                            
                                                return redirect('/op/login');
                                            }
                            }
                }else{
                    session()->flash('message.level', 'danger');
                    session()->flash('message.content', 'Your Account is deactivated by ADMIN');

                    return redirect('/op/login');
                }
            
        }else{

            session()->flash('message.level', 'danger');
            session()->flash('message.content', 'Your Account does not exist. Kindly contact with Admin');
            return redirect('/op/login');
        }
    }

    public function resetFirstTimePassword(Request $request)
    {
          $npass = $request->input('npass');
          $rpass = $request->input('rpass');
          $cmp_email = $request->input('cmp_email');

          if($npass != $rpass){
                session()->flash('message.level', 'warning');
                session()->flash('message.content', 'New Password is not matching.');
                return view('pages.op.resetPassword');
          }else{
             
               $pass =  Hash::make($npass);
                \DB::table('users')
                ->where('email', $cmp_email)
                ->update(['password' => $pass]);
    
                \DB::table('operators')
                ->where('email', $cmp_email)
                ->update(['password_status' => 1]);
                session()->flash('message.level', 'success');
                session()->flash('message.content', 'Successfully Password Reset');
                return redirect('/op/login');
          }
    }

    public function giveAttendence(){
        $incubatee_list = Incubatee::get();
        $attendence_list = Attendence::get();
        return view('pages.op.attendence')->with('incubatee_list',$incubatee_list)->with('attendence_list',$attendence_list);
    }

    public function saveAttendence(Request $request){
        $visitor_name = $request->input('visitor_name');
        $visitor_address = $request->input('visitor_address');
        $visitor_email = $request->input('visitor_email');
        $visitor_phn = $request->input('visitor_phn');
        $purpose = $request->input('purpose');
        $in_time =  date("H:i");
        $visitor_wtom = $request->input('visitor_wtom');
        $dt = $request->input('dt');
       
        // $start = Carbon::parse($in_time);
        // $end = Carbon::parse($out_time);

        // $total_time = $start->diffInMinutes($end); 
        // echo $total_time; die;

       

        $today_dt = date('Y-m-d');
        
            if($dt == $today_dt){
                            $attdnc = new Attendence();
                            $attdnc->visitor_name =  $visitor_name;
                            $attdnc->visitor_address = $visitor_address;
                            $attdnc->visitor_email    =  $visitor_email;
                            $attdnc->visitor_ph     =  $visitor_phn;
                            $attdnc->purpose      =  $purpose;
                            $attdnc->in_time      =  $in_time;
                            $attdnc->whoom_to_meet     =  $visitor_wtom;
                            $attdnc->date         =  $dt;
                            $attdnc->save();
                            session()->flash('message.level', 'success');
                            session()->flash('message.content', "Attendence Submitted");
                            return redirect()->back();                            

            }else{
                session()->flash('message.level', 'warning');
                session()->flash('message.content', "Today's date is not matching");
                return redirect()->back();
            }
    }

    function no_of_incubatee(){
        $count_incubatee = Incubatee::count();

        return $count_incubatee;
    }

    public function showDashboard()
    {
        $no_of_incubatee = $this->no_of_incubatee();
        return view('pages.op.dashboard')->with('no_of_incubatee',$no_of_incubatee);
    }

    public function profile(){
        $email = session()->get('opsession');
        $profile = Operators::where('email',$email)->first();
        return view('pages.op.profile')->with('profile_data',$profile);
    }

    public function profileEdit(){
        $email = session()->get('opsession');
        $profile = Operators::where('email',$email)->first();
        return view('pages.op.profileedit')->with('profile_edit_data',$profile);
    }

    public function profileSave(Request $request){
        $name = $request->input('op_name');
        $ph = $request->input('op_ph');
        $profile_img = $request->file('op_img');
        $email = session()->get('opsession');
        $id = $request->input('id');

        if($request->hasfile('op_img'))
        {
                 
                 $extension = $profile_img->getClientOriginalExtension();
                 $img = $email.'.'.$extension;
                //  \Storage::delete($img);
                 $profile_img->move(public_path().'/uploads/op/profile_pictures/', $img);  

                 $op_user = Operators::find($id);
                 $op_user->name  = $name;
                 $op_user->ph    =  $ph;
                 $op_user->img   =  $img;
                 $op_user->save();
                 session(['profile_img' => $img]);
                 $request->session()->flash('message.level', 'success');
                 $request->session()->flash('message.content', 'Operator updated Successfully !!!');
                 return redirect('/op/profile');
        }else{
                 $op_user = Operators::find($id);
                 $op_user->name  = $name;
                 $op_user->ph    = $ph;
                 $op_user->save();

                 $request->session()->flash('message.level', 'success');
                 $request->session()->flash('message.content', 'Operator updated Successfully !!!');
                 return redirect('/op/profile');
        }
    }

    public function passwordEdit(){
        return view('pages.op.passwordedit');
    }

    public function passwordSave(Request $request){
        $opass = $request->input('opass');
        $npass = $request->input('npass');
        $rpass = $request->input('rpass');
        $email = session()->get('opsession');

        $op_pass_chk = Users::where('email',$email)->first();
        if(Hash::check($opass,$op_pass_chk['password'])){
            
                    if($npass == $rpass){

                        $password = Hash::make($request->input('npass'));
                                \DB::table('users')
                                ->where('email', $email)
                                ->update(['password' => $password]);
                                session()->flash('message.level', 'success');
                                session()->flash('message.content', 'Password changed successfully');
                            return redirect()->back();
                    }else{
                        session()->flash('message.level', 'danger');
                        session()->flash('message.content', "Password does not match");
                        return redirect()->back();
                    }
        }else{
            session()->flash('message.level', 'danger');
            session()->flash('message.content', 'Old Password is wrong');
            return redirect()->back();
        }

    }

    public function showAttendence(){

        return view('pages.op.showattendenceone');
    } 

    public function showAttendenceByDate(){

        $today_dt = date('Y-m-d');
        $attdnc_sheet = Attendence::where('date',$today_dt)->get();
        // $attdnc_sheet = Attendence::get();

           return view('pages.op.showattendencetwo')->with('attdnc_sheet',$attdnc_sheet);

    }

    public function saveCheckoutTime(Request $request){
           $id = $request->input('id');
           $time_now =  date("H:i");
        //    echo $time_now; die;
           $checkout_time = Attendence::find($id);
           $checkout_time->out_time = $time_now;
           $checkout_time->checkout_status = "1";
           $checkout_time->save();

           return redirect()->back();
    }

    public function OpLogout(){
        \Session::forget('opsession');
        \Session::flush();
        return \Redirect::to('/op/login');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

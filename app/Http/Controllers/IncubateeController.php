<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Users;
use App\Incubatee;
use App\RequestForRoom;
use App\TimeSlot;
use App\RoomTransaction;

class IncubateeController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function loginFormShow()
    {
        return view('pages.incubatee.login');
    }

    
    public function loginCheck(Request $request)
    {
        $company_email = $request->input('username');
        $password = $request->input('password');

        $users = Users::where('email',$company_email)->first();

        if(!empty($users) && $users['status'] == 'incube')
        {
                if($users['account_status'] != 0){
                    $incub_pass_reset_check = Incubatee::where('company_email',$company_email)->first();
                            if($incub_pass_reset_check['password_status'] != 1){                                                                                          
                                    if(Hash::check($password,$users['password'])){
                                         return view('pages.incubatee.resetPassword',['cmp_email' => $company_email]);
                                    }else{
                                            session()->flash('message.level', 'danger');
                                            session()->flash('message.content', 'Wrong Username or Password');
                        
                                            return redirect('/');
                                    }
                            }else{
                                            if(Hash::check($password,$users['password'])){
                                                    
                                                session(['incubateesession' => $company_email,'name' => $users['company_name'],'profile_img' => $incub_pass_reset_check['company_logo']]);
                                                return redirect('/incubatee/dashboard');
                                            }else{
                                                session()->flash('message.level', 'danger');
                                                session()->flash('message.content', 'Wrong Username or Password');
                            
                                                return redirect('/');
                                            }
                            }
                }else{
                    session()->flash('message.level', 'danger');
                    session()->flash('message.content', 'Your Account is deactivated by ADMIN');

                    return redirect('/');
                }
            
        }else{

            session()->flash('message.level', 'danger');
            session()->flash('message.content', 'Your Account does not exist. Kindly contact with Admin');
            return redirect('/');
        }
    }

    public function profile(){
        $incubatee_profile = Incubatee::first();
        return view('pages.incubatee.profile')->with('incubatee_profile',$incubatee_profile);
    }
    public function profileEdit(){
        $incubatee_profile = Incubatee::first();
        return view('pages.incubatee.profileEdit')->with('incubatee_profile',$incubatee_profile);
    }
    
    public function profileUpdate(Request $request){
        $cmp_name = $request->input('cmp_name');
        $cmp_email = $request->input('cmp_email');
        $cmp_pan = $request->input('cmp_pan');
        $cmp_reg_no = $request->input('cmp_reg_no');
        $cmp_ph = $request->input('cmp_ph');
        $cmp_founders = $request->input('cmp_founders');
        $cmp_address = $request->input('cmp_address');
        $cmp_logo = $request->file('cmp_logo');
        $cmp_state = $request->input('cmp_state');
        $cmp_city = $request->input('cmp_city');
        $cmp_pincode = $request->input('cmp_pincode');
        
        $id = $request->input('id');

        if($request->hasfile('cmp_logo'))
        {
                 
                 $extension = $cmp_logo->getClientOriginalExtension();
                 $img = $cmp_name.'.'.$extension;
                //  \Storage::delete($img);
                 $cmp_logo->move(public_path().'/uploads/incubatee/profile_pictures/', $img);  

                 $incubatee_user = Incubatee::find($id);
                 $incubatee_user->company_name  = $cmp_name;
                 $incubatee_user->company_email    =  $cmp_email;
                 $incubatee_user->company_logo   =  $img;
                 $incubatee_user->pan_no  = $cmp_pan;
                 $incubatee_user->company_reg_no    =  $cmp_reg_no;
                 $incubatee_user->primary_ph  = $cmp_ph;
                 $incubatee_user->founders    =  $cmp_founders;
                 $incubatee_user->address  = $cmp_address;
                 $incubatee_user->state    =  $cmp_state;
                 $incubatee_user->city  = $cmp_city;
                 $incubatee_user->pincode    =  $cmp_pincode;

                 $incubatee_user->save();
                 session(['profile_img' => $img]);
                 $request->session()->flash('message.level', 'success');
                 $request->session()->flash('message.content', 'Incubatee updated Successfully !!!');
                 return redirect('/incubatee/profile');
        }else{
                 $incubatee_user = Incubatee::find($id);
                 $incubatee_user->company_name  = $cmp_name;
                 $incubatee_user->company_email    =  $cmp_email;
                 $incubatee_user->pan_no  = $cmp_pan;
                 $incubatee_user->company_reg_no    =  $cmp_reg_no;
                 $incubatee_user->primary_ph  = $cmp_ph;
                 $incubatee_user->founders    =  $cmp_founders;
                 $incubatee_user->address  = $cmp_address;
                 $incubatee_user->state    =  $cmp_state;
                 $incubatee_user->city  = $cmp_city;
                 $incubatee_user->pincode    =  $cmp_pincode;

                 $incubatee_user->save();

                 $request->session()->flash('message.level', 'success');
                 $request->session()->flash('message.content', 'Incubatee updated Successfully !!!');
                 return redirect('/incubatee/profile');
        }
    }

    public function editPassword(){
        return view('pages.incubatee.passwordedit');
    }

    public function updatePassword(Request $request){
        $opass = $request->input('opass');
        $npass = $request->input('npass');
        $rpass = $request->input('rpass');
        $email = session()->get('incubateesession');

        $pass_chk = Users::where('email',$email)->first();
        if(Hash::check($opass,$pass_chk['password'])){
            
                    if($npass == $rpass){

                        $password = Hash::make($request->input('npass'));
                                \DB::table('users')
                                ->where('email', $email) 
                                ->update(['password' => $password]);
                                session()->flash('message.level', 'success');
                                session()->flash('message.content', 'Password changed successfully');
                            return redirect()->back();
                    }else{
                        session()->flash('message.level', 'danger');
                        session()->flash('message.content', "Password does not match");
                        return redirect()->back();
                    }
        }else{
            session()->flash('message.level', 'danger');
            session()->flash('message.content', 'Old Password is wrong');
            return redirect()->back();
        }
    }
    public function resetFirstTimePassword(Request $request)
    {
        //   echo "done"; die;
          $npass = $request->input('npass');
          $rpass = $request->input('rpass');
          $cmp_email = $request->input('cmp_email');

          if($npass != $rpass){
                session()->flash('message.level', 'warning');
                session()->flash('message.content', 'New Password is not matching.');
                return view('pages.incubatee.resetPassword');
          }else{
  
               $pass =  Hash::make($npass);
                \DB::table('users')
                ->where('email', $cmp_email)
                ->update(['password' => $pass]);
    
                \DB::table('incubatee')
                ->where('company_email', $cmp_email)
                ->update(['password_status' => 1]);
                session()->flash('message.level', 'success');
                session()->flash('message.content', 'Successfully Password Reset');
                return redirect('/');
          }
    }

    public function RFTPassword(Request $request){
        echo "done"; die;
    }

    function pending_bookings(){
        $cmp_email = session()->get('incubateesession');
        $no_pending_bookings = RequestForRoom::where('company_email',$cmp_email)->where('req_status','0')->count();

        return $no_pending_bookings;
    }

    function accepted_bookings(){
        $cmp_email = session()->get('incubateesession');
        $no_accepted_bookings = RequestForRoom::where('company_email',$cmp_email)->where('req_status','1')->count();

        return $no_accepted_bookings;
    }

    function temp_accepted_bookings(){
        $cmp_email = session()->get('incubateesession');
        $temp_accepted_bookings = RequestForRoom::where('company_email',$cmp_email)->where('req_status','3')->count();

        return $temp_accepted_bookings;
    }

    public function showDashboard()
    {
        $no_pending_bookings = $this->pending_bookings();
        $no_accepted_bookings = $this->accepted_bookings();
        $temp_accepted_bookings = $this->temp_accepted_bookings();

        return view('pages.incubatee.dashboard')->with('temp_accepted_bookings',$temp_accepted_bookings)->with('pending_bookings',$no_pending_bookings)->with('accepted_bookings',$no_accepted_bookings);
    }

    public function createRequest(){
        return view('pages.incubatee.createarequest');
    }

    public function checkRequest(Request $request){
        $fdate = $request->input('fdate');
        $cmp_email = $request->input('cmp_email');
        $room = $request->input('room');
        $incubatee_email = $request->input('incubatee_email');
        $incubatee_name   = $request->input('incubatee_name');        
        $incubatee_ph   = $request->input('incubatee_ph');

        $booked_tym_slot = RoomTransaction::where('from_date',$fdate)
                                            ->where('room_type',$room)
                                            ->get();
        // print_r(count($booked_tym_slot)); die;
        $timings = TimeSlot::get();
        return view('pages.incubatee.bookedtiming')
               ->with("timings",$timings)
               ->with("booked_tym",$booked_tym_slot)
               ->with("room",$room)
               ->with("fdt",$fdate)
               ->with("incubatee_email",$incubatee_email)
               ->with("incubatee_name",$incubatee_name)
               ->with("incubatee_ph",$incubatee_ph);
    }

    public function saveRequest(Request $request){
        $dts = $request->input('tym');
        $fdate = $request->input('fdate');
        $cmp_email= $request->input('cmp_email');
        $room= $request->input('room');
        $purpose= $request->input('purpose');
        $incubatee_email = $request->input('incubatee_email');
        $incubatee_name = $request->input('incubatee_name');
        $incubatee_ph = $request->input('incubatee_ph');
        $count = 0;
        $total_min = 0;
        $from = 0;
        $to=0;
        $j=0;
        foreach($dts as $dt){
            $slotarray[] = TimeSlot::select('timings','id')->where('id',$dt)->first();
        }
        // Available alpha caracters
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // generate a pin based on 2 * 7 digits + a random character
        $pin = mt_rand(1000000, 9999999)
            . mt_rand(1000000, 9999999)
            . $characters[rand(0, strlen($characters) - 1)];

        // shuffle the result
        $string = str_shuffle($pin);

        foreach($slotarray as $sa){
            
        $save_req_slot = new RoomTransaction();
        $save_req_slot->cmp_email = $incubatee_email;
        $save_req_slot->added_by  = $cmp_email;
        $save_req_slot->from_date = $fdate;
        $save_req_slot->room_type = $room;
        $save_req_slot->notify_id = $incubatee_email."/".$fdate."/".$room."/".$string;
        $save_req_slot->time      = $sa['id'];
        $save_req_slot->save();
        $count =$count + 1;
        $tyms[$j] = $sa['timings'];
        $j++;
         
        }
        
        $save_room = new RequestForRoom();
        $save_room->from_date = $fdate;
        $save_room->to_date = "0000-00-00";
        $save_room->purpose = $purpose;
        $save_room->company_email = $incubatee_email;
        $save_room->company_name =  $incubatee_name;
        $save_room->company_ph   =  $incubatee_ph;
        $save_room->added_by  = $cmp_email;
        $save_room->from_time = "00:00";
        $save_room->to_time = "00:00";
        $save_room->notify_id = $incubatee_email."/".$fdate."/".$room."/".$string;
        $save_room->room = $room;
        $save_room->req_status = '1';
        $save_room->feedback = 'NA';
        $save_room->save();

        for($i=1;$i<=$count;$i++){
        $total_min = $total_min + 30; 
        }
        
        for($k=$count-1;$k>=0;$k--){
            $from = $tyms[$k];
            break;
        }

                                
                                $request->session()->flash('message.level', 'success');
                                $request->session()->flash('message.content', 'Request submitted successfully !!!');

        // $radio_val = $request->input('datecat');
        // $upper_limit = "09:00";
        // $lower_limit = "21:00";
        // switch($radio_val){
        //     case 1: 
                
        //             $room_check = RequestForRoom::where('from_date',$fdate)->get();  
                    
                    // foreach($room_check as $rc){
                    // $exist_ftime[] = $rc["from_time"];
                    // $exist_ttime[] = $rc["to_time"]; 
                    // }
                    // print_r($exist_ftime); die;
                    // $flag = 0;
                    
                    // foreach($room_check as $rc){
                    //     $exist_ftime = $rc["from_time"];
                    //     $exist_ttime = $rc["to_time"]; 
                    //  if($from_time < $upper_limit && $to_time < $lower_limit)   
            //           if($from_time < $exist_ftime && $to_time < $exist_ftime && $from_time > $exist_ttime && $to_time > $exist_ttime){
            //                 $flag=0;
                           
                        
            //         }else{
            //                 $flag = $flag + 1;
                            
            //         }
            //     }
            //                 if($flag > 0){
            //                     $request->session()->flash('fdate', $fdate);
            //                     $request->session()->flash('tdate', $tdate);
            //                     $request->session()->flash('from_time', $from_time);
            //                     $request->session()->flash('to_time', $to_time);
            //                     $request->session()->flash('room', $room);
            //                     $request->session()->flash('purpose', $purpose);
            //                     $request->session()->flash('radio_val', $radio_val);
            //                     // $request->session()->flash('message.content', 'Error');
            //                     $request->session()->flash('message.level', 'warning');
            //                     $request->session()->flash('message.content', 'This slot is occupied');
        
            //                     return redirect()->back();
            //                 }else{
            //                     $save_room = new RequestForRoom();
            //                     $save_room->from_date = $fdate;
            //                     $save_room->to_date = "0000-00-00";
            //                     $save_room->purpose = $purpose;
            //                     $save_room->company_email = $cmp_email;
            //                     $save_room->from_time = $from_time;
            //                     $save_room->to_time = $to_time;
            //                     $save_room->room = $room;
            //                     $save_room->req_status = '0';
            //                     $save_room->feedback = 'NA';
            //                     $save_room->save();
            //                     $request->session()->flash('message.level', 'success');
            //                     $request->session()->flash('message.content', 'Request submitted successfully !!!');
    
            //                     return redirect('/incubatee/request/create');
                                
            //                 }

            // break;
            // case 2: 
            //         if($tdate != null){
            //             $save_room = new RequestForRoom();
            //             $save_room->from_date = $fdate;
            //             $save_room->to_date = $tdate;
            //             $save_room->purpose = $purpose;
            //             $save_room->company_email = $cmp_email;
            //             $save_room->from_time = $from_time;
            //             $save_room->to_time = $to_time;
            //             $save_room->room = $room;
            //             $save_room->req_status = '0';
            //             $save_room->feedback = 'NA';
            //             $save_room->save();
            //             $request->session()->flash('message.level', 'success');
            //             $request->session()->flash('message.content', 'Request submitted successfully !!!');

            //             return redirect('/incubatee/request/create');
            //         }else{
            //             $save_room = new RequestForRoom();
            //             $save_room->from_date = $fdate;
            //             $save_room->to_date = "0000-00-00";
            //             $save_room->purpose = $purpose;
            //             $save_room->company_email = $cmp_email;
            //             $save_room->from_time = $from_time;
            //             $save_room->to_time = $to_time;
            //             $save_room->room = $room;
            //             $save_room->req_status = '0';
            //             $save_room->feedback = 'NA';
            //             $save_room->save();
            //             $request->session()->flash('message.level', 'success');
            //             $request->session()->flash('message.content', 'Request submitted successfully !!!');

            //             return redirect('/incubatee/request/create');
            //         }
            // break;
            // default: 
            // $request->session()->flash('message.level', 'warning');
            // $request->session()->flash('message.content', 'Error');
    
            return redirect('/op/request/create');

    
    } 

     public function showRequestList(){
        $cmp_email = session()->get('opsession');
        $req_list  = RequestForRoom::where('added_by',$cmp_email)->get();
        $incubatee_list =  Incubatee::get();
        $slot_list = RoomTransaction::get();
        $slot_id = TimeSlot::get();

        return view('pages.incubatee.roomRequestList',['req_list' => $req_list])
                    ->with('incubateelist',$incubatee_list)
                    ->with('slot_list',$slot_list)
                    ->with('slot_id',$slot_id);
    }
 
    public function updateRoomReqInfo(Request $request){
        $id= $request->input('id');
        $notify_id = $request->input('notify_id');
        $fdate= $request->input('fdate');
        $cmp_email= $request->input('incubatee_email');
        $room= $request->input('room');
        $time_id= $request->input('tym');
        $purpose= $request->input('purpose');
        $slt_id = $request->input('slt_id');        
        $tym_id = array();
        $slts   = array();
        
        $room_req_update = RequestForRoom::find($id);
        $room_req_update->from_date  =  $fdate;
        $room_req_update->to_date    =  "0000-00-00";
        $room_req_update->purpose    =  $purpose;
        // $room_req_update->company_email = $cmp_email;
        $room_req_update->from_time  =  "00:00";
        $room_req_update->to_time    =  "00:00";
        $room_req_update->room       =  $room;
        $room_req_update->save();
  
        
        foreach($time_id as $tym){
            $tym_id[] = $tym;
        }
        foreach($slt_id as $slid){
            $slts[] = $slid;
        }
           
            for($i=0;$i<count($slts);$i++){
               //slot_booking_update
               RoomTransaction::where('id',$slts[$i])
                                ->delete(); //(['time'=>$tym_id[$i]]);
            }

        for($k=0;$k<count($tym_id);$k++){
        $room_trans = new RoomTransaction();
        $room_trans->notify_id = $notify_id;
        // $room_trans->cmp_email = $cmp_email;
        $room_trans->from_date = $fdate;
        $room_trans->room_type = $room;
        $room_trans->time      = $tym_id[$k];
        $room_trans->save();
        }

        session()->flash('message.level', 'success');
        session()->flash('message.content', 'Room Request is Updated');

        return redirect('/op/request/list');
    }

    public function deleteRoomReqInfo(Request $request){
           $room_req_id = $request->input('id');
           $slot_id     = $request->input('slot_id');

           for($i=0;$i<count($slot_id);$i++){
            //slot_booking_update
            RoomTransaction::where('id',$slot_id[$i])
                             ->delete(); //(['time'=>$tym_id[$i]]);
         }

         RequestForRoom::where('id',$room_req_id)
                          ->delete();
        return redirect('/op/request/list');
    }

    public function IncubateeLogout(){
        \Session::forget('incubateesession');
        \Session::flush();
        return \Redirect::to('/');
    }

}

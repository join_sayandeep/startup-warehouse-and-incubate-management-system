<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Users;
use App\Operators;
use App\Incubatee;
use App\RequestForRoom;
use Carbon\Carbon;
use App\Attendence;

class GuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ShowLogin()
    {
        return view('pages.guest.login');
    }

    public function CheckLogin(Request $request)
    {
        $company_email = $request->input('username');
        $password = $request->input('password');
        
        $pass = Hash::make($request->input('password'));
        // echo $pass; die;

        $users = Users::where('email',$company_email)->first();

        if(!empty($users) && $users['status'] == 'guest')
        {
                                            if(Hash::check($password,$users['password'])){
                                                    
                                                session(['gsession' => $company_email,'name' => $users['company_name']]);
                                                return redirect('/guest/attendence');
                                            }else{
                                                session()->flash('message.level', 'danger');
                                                session()->flash('message.content', 'Wrong Username or Password');
                            
                                                return redirect('/guest/login');
                                            }
            
        }else{

            session()->flash('message.level', 'danger');
            session()->flash('message.content', 'Your Account does not exist. Kindly contact with Admin');
            return redirect('/guest/login');
        }
    }

    
    public function ShowAttendence()
    {
        $incubatee_list = Incubatee::get();
        $attendence_list = Attendence::get();

        return view('pages.guest.attendence')->with('incubatee_list',$incubatee_list)->with('attendence_list',$attendence_list);
    }

    public function saveAttendence(Request $request){
        $visitor_name = $request->input('visitor_name');
        $visitor_address = $request->input('visitor_address');
        $visitor_email = $request->input('visitor_email');
        $visitor_phn = $request->input('visitor_phn');
        $purpose = $request->input('purpose');
        $in_time =  date("H:i");
        $visitor_wtom = $request->input('visitor_wtom');
        $dt = $request->input('dt');
       
        $today_dt = date('Y-m-d');
        
            if($dt == $today_dt){
                            $attdnc = new Attendence();
                            $attdnc->visitor_name =  $visitor_name;
                            $attdnc->visitor_address = $visitor_address;
                            $attdnc->visitor_email    =  $visitor_email;
                            $attdnc->visitor_ph     =  $visitor_phn;
                            $attdnc->purpose      =  $purpose;
                            $attdnc->in_time      =  $in_time;
                            $attdnc->whoom_to_meet     =  $visitor_wtom;
                            $attdnc->date         =  $dt;
                            $attdnc->save();
                            session()->flash('message.level', 'success');
                            session()->flash('message.content', "Attendence Submitted");
                            return redirect()->back();                            

            }else{
                session()->flash('message.level', 'warning');
                session()->flash('message.content', "Today's date is not matching");
                return redirect()->back();
            }
    }

    public function showAttendenceByDate()
    {
        
        $today_dt = date('Y-m-d');
        $attdnc_sheet = Attendence::where('date',$today_dt)->get();
        // $attdnc_sheet = Attendence::get();

           return view('pages.guest.showattendencetwo')->with('attdnc_sheet',$attdnc_sheet);

    }

    public function saveCheckoutTime(Request $request){
        $id = $request->input('id');
        $time_now =  date("H:i");
     //    echo $time_now; die;
        $checkout_time = Attendence::find($id);
        $checkout_time->out_time = $time_now;
        $checkout_time->checkout_status = "1";
        $checkout_time->save();

        return redirect()->back();
 }
   public function RecInput(){
       return view('rec');
   }

   public function RecInputSave(Request $request){
    $visitor_name = $request->input('visitor_name');
    $visitor_address = $request->input('visitor_address');
    $visitor_email = $request->input('visitor_email');
    $visitor_phn = $request->input('visitor_phn');
    $purpose = $request->input('purpose');
    $in_time =  $request->input('in');
    $out_time =  $request->input('out');
    $visitor_wtom = $request->input('visitor_wtom');
    $dt = $request->input('dt');
   
                        $attdnc = new Attendence();
                        $attdnc->visitor_name =  $visitor_name;
                        $attdnc->visitor_address = $visitor_address;
                        $attdnc->visitor_email    =  $visitor_email;
                        $attdnc->visitor_ph     =  $visitor_phn;
                        $attdnc->purpose      =  $purpose;
                        $attdnc->in_time      =  $in_time;
                        $attdnc->out_time     = $out_time;
                        $attdnc->whoom_to_meet     =  $visitor_wtom;
                        $attdnc->date         =  $dt;
                        $attdnc->save();
                        session()->flash('message.level', 'success');
                        session()->flash('message.content', "Attendence Submitted");
                        return redirect()->back();                            

       
   }
}

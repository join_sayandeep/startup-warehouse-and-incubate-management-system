<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Users;
use App\Operators;
use App\Incubatee;
use App\RequestForRoom;
use Carbon\Carbon;
use App\Attendence;
use App\RoomTransaction;
use App\TimeSlot;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function loginFormShow()
    {
        return \View::make('pages.admin.login');
    }

    
    public function loginCheck(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');

        $users = Users::where('email',$username)->first();
        
        if(!empty($users) && $users['status'] == 'admin')
        {
                if($users['password'] == $password) { //Hash::check($password,$users['password'])){
                    
                    session(['adminsession' => $username,'name' => $users['name']]);
                    return redirect("/admin/dashboard");
                }else{
                    $request->session()->flash('message.level', 'danger');
                    $request->session()->flash('message.content', 'Wrong Username & Password !!!');
                    return redirect("/admin/login");
                }   

        }else if(!empty($users) && $users['status'] == 'op'){
                     return redirect("/op/login");
        }else{
                     return redirect("/incubatee/login");
        }
    }
 

    public function showDashboard()
    {
        $no_of_incubatee = $this->no_of_incubatee();
        $no_of_operator = $this->no_of_operator();
        $pending_bookings = $this->pending_bookings();
        $accepted_bookings = $this->accepted_bookings();
        $not_accepted_bookings = $this->not_accepted_bookings();
        $temp_accepted_bookings = $this->temp_accepted_bookings();

        return view("pages.admin.dashboard")
               ->with('no_of_operator',$no_of_operator)
               ->with('no_of_incubatee',$no_of_incubatee)
               ->with('pending_bookings',$pending_bookings)
               ->with('accepted_bookings',$accepted_bookings)
               ->with('not_accepted_bookings',$not_accepted_bookings)
               ->with('temp_accepted_bookings',$temp_accepted_bookings);
    }

    function no_of_operator(){
        $count_op = Operators::count();

        return $count_op;
    }
    function no_of_incubatee(){
        $count_incubatee = Incubatee::count();

        return $count_incubatee;
    }

    function pending_bookings(){
        $no_pending_bookings = RequestForRoom::where('req_status','0')->count();

        return $no_pending_bookings;
    }

    function accepted_bookings(){
        $no_accepted_bookings = RequestForRoom::where('req_status','1')->count();

        return $no_accepted_bookings;
    }
    
    function not_accepted_bookings(){
        $not_accepted_bookings = RequestForRoom::where('req_status','2')->count();

        return $not_accepted_bookings;
    }

    function temp_accepted_bookings(){
        $not_accepted_bookings = RequestForRoom::where('req_status','3')->count();

        return $not_accepted_bookings;
    }

    public function addOperatorsForm()
    {
        return view("pages.admin.addOperators");
    }
   
    public function saveOperators(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'Email Id' => 'unique:operators,email',
            'Password' => 'max:12|min:9',
            'Phone'   => 'digits:10',
            'Profile Image' => 'image|mimes:jpeg,jpg,png',
        ]);

        if ($validator->fails()) {
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Something went wrong !!!');
            return redirect('/admin/create/operator/add')
                        ->withErrors($validator)
                        ->withInput();
        }else{
               //Operator Information saving to DB
            
          
               $email = $request->input('op_email');
               $name = $request->input('op_name');
               $password = Hash::make($request->input('op_password'));
            //    Hash::check($request->newPasswordAtLogin, $hashedPassword)
               $phn = $request->input('op_phn');
               $profile_pic = $request->file('op_profile_pic');

              //Check Operator existing
              $op_existing_user = Operators::where('email',$email)->count();

            if($op_existing_user > 0){
                    
                $request->session()->flash('message.level', 'warning');
                $request->session()->flash('message.content', 'Operator already exist');
                return redirect('/admin/create/operator/add');
            }else{

               if($request->hasfile('op_profile_pic'))
               {
                        $extension = $profile_pic->getClientOriginalExtension();
                        $img = $email.'.'.$extension;
                        $profile_pic->move(public_path().'/uploads/op/profile_pictures/', $img);  

                        $op_user = new Operators();
                        $op_user->email = $email;
                        $op_user->name  = $name;
                        $op_user->password_status = '0';
                        $op_user->ph    =    $phn;
                        $op_user->img   =    $img;
                        $op_user->save();

                        $user = new Users();
                        $user->name = $name;
                        $user->email= $email;
                        $user->password=$password;
                        $user->status = 'op';
                        $user->save();
                        $request->session()->flash('message.level', 'success');
                        $request->session()->flash('message.content', 'Operator added Successfully !!!');
                        return redirect('/admin/create/operator/add');
               }else{
                        $op_user = new Operators();
                        $op_user->email = $email;
                        $op_user->name  = $name;
                        $op_user->password_status = '0';
                        $op_user->ph    =    $phn;
                        $op_user->img   =    "profile_pic.png";
                        $op_user->save();

                        $user = new Users();
                        $user->name = $name;
                        $user->email= $email;
                        $user->password=$password;
                        $user->status = 'op';
                        $user->save();
                        $request->session()->flash('message.level', 'success');
                        $request->session()->flash('message.content', 'Operator added Successfully !!!');
                        return redirect('/admin/create/operator/add');
               }
            }

        }
    }

    public function showOperatorsList()
    {
        // $operators = Operators::orderBy('created_at','DESC')->get();

        $operators = \DB::table('users')
            ->join('operators', 'users.email', '=', 'operators.email')
            ->select('users.*', 'operators.*')
            ->get();

        return view('pages.admin.showOperators')->with('operators',$operators);
    }

    public function DeactiveOperator($email){
         
        \DB::table('users')
            ->where('email', $email)
            ->update(['account_status' => 0]);

            session()->flash('message.level', 'success');
            session()->flash('message.content', 'Operator has been deactivated.');

            return redirect('/admin/create/operator/show');
    }

    public function ActiveOperator($email){
         
        \DB::table('users')
            ->where('email', $email)
            ->update(['account_status' => 1]);

            session()->flash('message.level', 'success');
            session()->flash('message.content', 'Operator has been activated.');

            return redirect('/admin/create/operator/show');
    }

    public function addIncubateeForm()
    {
        return view('pages.admin.addIncubatee');
    }

    public function saveIncubatees(Request $request){
               //Incubatee Information saving to DB

               $cmp_name = $request->input('cmp_name');
               $cmp_email = $request->input('cmp_email');
               $cmp_password = Hash::make($request->input('cmp_password'));
               $cmp_pan = $request->input('cmp_pan');
               $cmp_reg_no = $request->input('cmp_reg_no');
               $cmp_ph = $request->input('cmp_ph');
               $cmp_logo = $request->file('cmp_logo');
               $cmp_founders = $request->input('cmp_founders');
               $cmp_address = $request->input('cmp_address');
               $cmp_state = $request->input('cmp_state');
               $cmp_city = $request->input('cmp_city');
               $cmp_pincode = $request->input('cmp_pincode');               
               $cmp_sdt = $request->input('cmp_sdt');
               $cmp_edt = $request->input('cmp_edt');

         //Check Incubator existing
         $incub_existing_user = Incubatee::where('company_email',$cmp_email)->count();

         if($incub_existing_user > 0){
                 
             $request->session()->flash('message.level', 'warning');
             $request->session()->flash('message.content', 'Incubatee already exist');
             return redirect('/admin/create/incubatee/add');
         }else{

               if($request->hasfile('cmp_logo'))
               {
                        $extension = $cmp_logo->getClientOriginalExtension();
                        $img = $cmp_name.'.'.$extension;
                        $cmp_logo->move(public_path().'/uploads/incubatee/profile_pictures/', $img);  

                        $incub_user = new Incubatee();
                        $incub_user->company_name = $cmp_name;
                        $incub_user->company_email  = $cmp_email;
                        $incub_user->password_status = '0';
                        $incub_user->pan_no    =    $cmp_pan;
                        $incub_user->company_logo   =   $img;
                        $incub_user->company_reg_no = $cmp_reg_no;
                        $incub_user->primary_ph = $cmp_ph;
                        $incub_user->founders = $cmp_founders;
                        $incub_user->start_dt = $cmp_sdt;
                        $incub_user->end_dt = $cmp_edt;
                        $incub_user->address = $cmp_address;
                        $incub_user->state = $cmp_state;
                        $incub_user->city = $cmp_city;
                        $incub_user->pincode = $cmp_pincode;

                        $incub_user->save();

                        $user = new Users();
                        $user->name = $cmp_name;
                        $user->email= $cmp_email;
                        $user->password=$cmp_password;
                        $user->status = 'incube';
                        $user->save();
                        $request->session()->flash('message.level', 'success');
                        $request->session()->flash('message.content', 'Incubatee added Successfully !!!');
                        return redirect('/admin/create/incubatee/add');
               }else{
                        $incub_user = new Incubatee();
                        $incub_user->company_name = $cmp_name;
                        $incub_user->company_email  = $cmp_email;
                        $incub_user->password_status = '0';
                        $incub_user->pan_no    =    $cmp_pan;
                        $incub_user->company_logo   =   "profile_pic.png";
                        $incub_user->company_reg_no = $cmp_reg_no;
                        $incub_user->primary_ph = $cmp_ph;
                        $incub_user->founders = $cmp_founders;
                        $incub_user->start_dt = $cmp_sdt;
                        $incub_user->end_dt = $cmp_edt;
                        $incub_user->address = $cmp_address;
                        $incub_user->state = $cmp_state;
                        $incub_user->city = $cmp_city;
                        $incub_user->pincode = $cmp_pincode;
                        $incub_user->save();

                        $user = new Users();
                        $user->name = $cmp_name;
                        $user->email= $cmp_email;
                        $user->password=$cmp_password;
                        $user->status = 'incube';
                        $user->save();
                        $request->session()->flash('message.level', 'success');
                        $request->session()->flash('message.content', 'Incubatee added Successfully !!!');
                        return redirect('/admin/create/incubatee/add');
               }
            }
        }

        public function showIncubateesList(){

            $incubatee = \DB::table('users')
            ->join('incubatee', 'users.email', '=', 'incubatee.company_email')
            ->select('users.*', 'incubatee.*')
            ->get();

            return view('pages.admin.showIncubatee')->with('incubatee',$incubatee);
        }

        
    public function DeactiveIncubatee($email){
         
        \DB::table('users')
            ->where('email', $email)
            ->update(['account_status' => 0]);

            session()->flash('message.level', 'success');
            session()->flash('message.content', 'Incubatee has been deactivated.');

            return redirect('/admin/create/incubatee/show');
    }

    public function ActiveIncubatee($email){
         
        \DB::table('users')
            ->where('email', $email)
            ->update(['account_status' => 1]);

            session()->flash('message.level', 'success');
            session()->flash('message.content', 'Incubatee has been activated.');

            return redirect('/admin/create/incubatee/show');
    }

    public function reqRoomList(){

         $req_room_list = \DB::table('requests_for_room')->latest()->get();
         
         $time_slots = TimeSlot::get();
         $slot_list = array();
         foreach($req_room_list as $rlid){
            $slot_list = RoomTransaction::where('notify_id',$rlid->notify_id)->latest()->get();
         }
         
         $users = \DB::table('room_transaction')
            ->join('timeslot', 'timeslot.id', '=', 'room_transaction.time')
            ->select('room_transaction.*', 'timeslot.*')
            ->get();
  
        // print_r($users); die;
        return view('pages.admin.requestaroomlist')->with('users',$users)->with('req_room_list',$req_room_list)->with('slot_list',$slot_list)->with('time_slots',$time_slots);
    }
 
    public function statusChangeReqRoom(Request $request){

        $feedback = 'NA';
        $status = $request->input('status');
        $id = $request->input('id');

        switch ($status) {
            case '1':
                        $room_req_update = RequestForRoom::find($id);
                        $room_req_update->req_status  = $status;
                        $room_req_update->feedback  =  $feedback;
                        $room_req_update->save(); 
    
                break;

            case '2':
                        $room_req_update = RequestForRoom::find($id);
                        $room_req_update->req_status  = $status;
                        $room_req_update->feedback  =  $feedback;
                        $room_req_update->save(); 
                break;
            case '3':
                        $room_req_update = RequestForRoom::find($id);
                        $room_req_update->req_status  = $status;
                        $room_req_update->feedback  =  $feedback;
                        $room_req_update->save(); 
                break;
            
            default:
                        $room_req_update = RequestForRoom::find($id);
                        $room_req_update->req_status  = "0";
                        $room_req_update->feedback  =  "NA";
                        $room_req_update->save(); 
                break;
        }
            return redirect()->back();
    }

    public function statusApprovedList($status){
        
        switch ($status) {
            case '1':
                         $approved_list = RequestForRoom::where('req_status','1')->get();
                         $users = \DB::table('room_transaction')
                         ->join('timeslot', 'timeslot.id', '=', 'room_transaction.time')
                         ->select('room_transaction.*', 'timeslot.*')
                         ->get();
                         return view('pages.admin.approvedList')->with('categorized_list',$approved_list)->with('users',$users)->with('class','green');
                break;
            
            case '2':
                         $nonapproved_list = RequestForRoom::where('req_status','2')->get();
                         $users = \DB::table('room_transaction')
                         ->join('timeslot', 'timeslot.id', '=', 'room_transaction.time')
                         ->select('room_transaction.*', 'timeslot.*')
                         ->get();
                         return view('pages.admin.notapprovedList')->with('categorized_list',$nonapproved_list)->with('users',$users)->with('class','red');
                break;
            case '3':
                         $tempapproved_list = RequestForRoom::where('req_status','3')->get();
                         $users = \DB::table('room_transaction')
                         ->join('timeslot', 'timeslot.id', '=', 'room_transaction.time')
                         ->select('room_transaction.*', 'timeslot.*')
                         ->get();
                         return view('pages.admin.tempapprovedList')->with('categorized_list',$tempapproved_list)->with('users',$users)->with('class','blue');
                break;
        }
    }

    public function BookaHall(){
                
        return view('pages.admin.bookahall');
    }

    public function checkBookedHall(Request $request){
        $fdate = $request->input('fdate');
        $cmp_email = $request->input('cmp_email');
        $room = $request->input('room');
        $booked_tym_slot = RoomTransaction::where('from_date',$fdate)
                                            ->where('room_type',$room)
                                            ->get();
        // print_r(count($booked_tym_slot)); die;
        $timings = TimeSlot::get();
        return view('pages.admin.bookedtiming')->with("timings",$timings)->with("booked_tym",$booked_tym_slot)->with("room",$room)->with("fdt",$fdate);
    }

    public function saveHallBookingInfo(Request $request){
        $dts = $request->input('tym');
        $fdate = $request->input('fdate');
        $cmp_email= $request->input('cmp_email');
        $room= $request->input('room');
        $purpose= $request->input('purpose');
        $count = 0;
        $total_min = 0;
        $from = 0;
        $to=0;
        $j=0;
        foreach($dts as $dt){
            $slotarray[] = TimeSlot::select('timings','id')->where('id',$dt)->first();
        }
        // Available alpha caracters
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // generate a pin based on 2 * 7 digits + a random character
        $pin = mt_rand(1000000, 9999999)
            . mt_rand(1000000, 9999999)
            . $characters[rand(0, strlen($characters) - 1)];

        // shuffle the result
        $string = str_shuffle($pin);

        foreach($slotarray as $sa){
            
        $save_req_slot = new RoomTransaction();
        $save_req_slot->cmp_email = $cmp_email;
        $save_req_slot->from_date = $fdate;
        $save_req_slot->room_type = $room;
        $save_req_slot->notify_id = $cmp_email."/".$fdate."/".$room."/".$string;
        $save_req_slot->time      = $sa['id'];
        $save_req_slot->save();
        $count =$count + 1;
        $tyms[$j] = $sa['timings'];
        $j++;
         
        }
        
        $save_room = new RequestForRoom();
        $save_room->from_date = $fdate;
        $save_room->to_date = "0000-00-00";
        $save_room->purpose = $purpose;
        $save_room->company_email = $cmp_email;
        $save_room->from_time = "00:00";
        $save_room->to_time = "00:00";
        $save_room->notify_id = $cmp_email."/".$fdate."/".$room."/".$string;
        $save_room->room = $room;
        $save_room->req_status = '0';
        $save_room->feedback = 'NA';
        $save_room->save();

        for($i=1;$i<=$count;$i++){
        $total_min = $total_min + 30; 
        }
        
        for($k=$count-1;$k>=0;$k--){
            $from = $tyms[$k];
            break;
        }

                                
                                $request->session()->flash('message.level', 'success');
                                $request->session()->flash('message.content', 'Request submitted successfully !!!');

    
            return redirect('/admin/booking/own/req');

    
    }


    public function showRoomBookingList(){
        $cmp_email = session()->get('adminsession');
        $req_list  = RequestForRoom::where('company_email',$cmp_email)->get();

        $slot_list = RoomTransaction::get();
        $slot_id = TimeSlot::get();

        return view('pages.admin.showhallbookinglist',['req_list' => $req_list])->with('slot_list',$slot_list)->with('slot_id',$slot_id);
    }

    public function updateHallBookingInfo(Request $request){
        $id= $request->input('id');
        $notify_id = $request->input('notify_id');
        $fdate= $request->input('fdate');
        $cmp_email= $request->input('cmp_email');
        $room= $request->input('room');
        $time_id= $request->input('tym');
        $purpose= $request->input('purpose');
        $slt_id = $request->input('slt_id');        
        $tym_id = array();
        $slts   = array();
        
        $room_req_update = RequestForRoom::find($id);
        $room_req_update->from_date  =  $fdate;
        $room_req_update->to_date    =  "0000-00-00";
        $room_req_update->purpose    =  $purpose;
        $room_req_update->from_time  =  "00:00";
        $room_req_update->to_time    =  "00:00";
        $room_req_update->room       =  $room;
        $room_req_update->save();
  
        
        foreach($time_id as $tym){
            $tym_id[] = $tym;
        }
        foreach($slt_id as $slid){
            $slts[] = $slid;
        }
           
            for($i=0;$i<count($slts);$i++){
               //slot_booking_update
               RoomTransaction::where('id',$slts[$i])
                                ->delete(); //(['time'=>$tym_id[$i]]);
            }

        for($k=0;$k<count($tym_id);$k++){
        $room_trans = new RoomTransaction();
        $room_trans->notify_id = $notify_id;
        $room_trans->cmp_email = $cmp_email;
        $room_trans->from_date = $fdate;
        $room_trans->room_type = $room;
        $room_trans->time      = $tym_id[$k];
        $room_trans->save();
        }

        session()->flash('message.level', 'success');
        session()->flash('message.content', 'Room Request is Updated');

        return redirect()->back();
    }

    public function restoreHallBooking(Request $request){
        $id = $request->input('id');
        $room_req_update = RequestForRoom::find($id);
        $room_req_update->restore_feedback  = $request->input('feedback');
        $room_req_update->req_status  = '0';
        $room_req_update->save(); 

        session()->flash('message.level', 'success');
        session()->flash('message.content', 'Hall request restored');
        
        return redirect()->back();
          
    }

    public function deleteHallBookingInfo(Request $request){
        $room_req_id = $request->input('id');
           $slot_id     = $request->input('slot_id');

           for($i=0;$i<count($slot_id);$i++){
            //slot_booking_update
            RoomTransaction::where('id',$slot_id[$i])
                             ->delete(); //(['time'=>$tym_id[$i]]);
         }

         RequestForRoom::where('id',$room_req_id)
                          ->delete();

        return redirect()->back();
    }
    public function showAttendence(){

        return view('pages.admin.showattendenceone');
    } 

    public function reqRoomShow(){
        return view('pages.admin.showbookinglist');
    }
    public function reqRoomByRangeDate(Request $request){

        $carbon_today_dt = Carbon::today()->toDateString();
        $freq_dt     = $request->input('fdt');
        $treq_dt     = Carbon::parse($request->input('tdt'))->toDateString();
        
           $booking_sheet = RequestForRoom::whereBetween('from_date', [$freq_dt, $treq_dt])->get();
        $timeslots= array();
        $time_id = array();

        if(!empty($booking_sheet)){
          foreach($booking_sheet as $book){
              $time_id = RoomTransaction::get();
           }
          

          foreach($time_id as $t){
            $timeslots = TimeSlot::select('timings','id')->get();
          }
        }else{
            $booking_sheet=array();
            $time_id=array();
            $timeslots= array();
        }
           
           return view('pages.admin.approvedList')
                  ->with('booking_sheet',$booking_sheet)
                  ->with('time_id',$time_id)
                  ->with('timeslots',$timeslots);


    }
    public function showAttendenceByDate(Request $request){

        $carbon_today_dt = Carbon::today()->toDateString();
        $freq_dt     = $request->input('fdt');
        $treq_dt     = Carbon::parse($request->input('tdt'))->toDateString();
        // // echo Carbon::parse($req_dt)->toDateString(); die;
        // if($carbon_today_dt < Carbon::parse($req_dt)->toDateString() ){
        //     session()->flash('message.level', 'danger');
        //     session()->flash('message.content', "No records found");
        //     return redirect()->back();
        // }else{
        //  echo "saved"; die;
           $attdnc_sheet = Attendence::whereBetween('date', [$freq_dt, $treq_dt])->get();
           
           return view('pages.admin.showattendencetwo')->with('attdnc_sheet',$attdnc_sheet);

        // }
    }
    public function AdminLogout(){
        \Session::forget('adminsession');
        \Session::flush();
        return \Redirect::to('/admin/login');
    }

}
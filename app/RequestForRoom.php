<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestForRoom extends Model
{
    protected $table = 'requests_for_room';
}

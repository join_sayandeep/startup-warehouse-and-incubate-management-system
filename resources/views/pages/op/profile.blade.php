@extends('layouts.op.opDashboard')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <!-- <h2>BLANK PAGE</h2> -->
            </div>

<div class="row clearfix">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Profile
                                <!-- <small>The default media displays a media object (images, video, audio) to the left or right of a content block.</small> -->
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="/op/profile/edit" class=" waves-effect waves-block">Edit</a></li>
                                        <!-- <li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>-->
                                        <li><a href="/op/password/update" class=" waves-effect waves-block">Change Password</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="media">
                                <div class="media-left">
                                    <a href="javascript:void(0);">
                                        <img class="media-object" src="{{ asset('uploads/op/profile_pictures/') }}/{{ $profile_data['img'] }}" width="64" height="64">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading">{{ $profile_data['name'] }}</h4>
                                    <h4 class="media-heading">{{ $profile_data['ph'] }}</h4>
                                    <h4 class="media-heading">{{ $profile_data['email'] }}</h4>
                                    
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-left">
                                    <a href="#">
                                        <!-- <img class="media-object" src="http://placehold.it/64x64" width="64" height="64"> -->
                                    </a>
                                </div>
                                <div class="media-body">
                                    <!-- <h4 class="media-heading">Media heading</h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque
                                    ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra
                                    turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis
                                    in faucibus.
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <img class="media-object" src="http://placehold.it/64x64" width="64" height="64">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading">Nested media heading</h4> Cras sit amet nibh libero, in gravida nulla. Nulla
                                            vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum
                                            in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate
                                            fringilla. Donec lacinia congue felis in faucibus.
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                            <div class="media">
                                <!-- <div class="media-body">
                                    <h4 class="media-heading">Media heading</h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque
                                    ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra
                                    turpis.
                                </div>
                                <div class="media-right">
                                    <a href="#">
                                        <img class="media-object" src="http://placehold.it/64x64" width="64" height="64">
                                    </a>
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-left">
                                    <a href="#">
                                        <img class="media-object" src="http://placehold.it/64x64" width="64" height="64">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading">Media heading</h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque
                                    ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra
                                    turpis.
                                </div>
                                <div class="media-right">
                                    <a href="#">
                                        <img class="media-object" src="http://placehold.it/64x64" width="64" height="64">
                                    </a>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>








        </div>
    </section>


@endsection
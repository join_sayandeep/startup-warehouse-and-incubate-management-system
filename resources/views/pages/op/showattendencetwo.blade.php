@extends('layouts.op.opDashboard')

@section('content')
<section class="content">
        <div class="container-fluid">
            <div class="block-header">
            <button type="button" class="btn btn-success btn-rounded m-b-10 m-l-5" data-toggle="modal" data-target="#VisitorRecord"><i class="fa fa-plus" aria-hidden="true"></i> Add</button>
            @include('pages.op.inputvisitorrecordsmodel')
                                                                             
            </div>

 <!-- Exportable Table -->
 <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                    @if(session()->has('message.level'))
                        <div class="alert alert-{{ session('message.level') }}"> 
                        {!! session('message.content') !!}
                        </div>
                    @endif
                        <div class="header">
                            <h2>
                              Today's Visitors List
                            </h2>
                            <!-- <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul> -->
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Visitor Name</th>
                                            <th>Visitor Address</th>
                                            <th>Visitor Email-Id</th>
                                            <th>Visitor Ph#</th>
                                            <th>Program Details</th>
                                            <th>Whoom to Meet</th>
                                            <th>IN Time</th>
                                            <th>OUT Time</th>                                            
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Visitor Name</th>
                                            <th>Visitor Address</th>
                                            <th>Visitor Email-Id</th>
                                            <th>Visitor Ph#</th>
                                            <th>Program Details</th>
                                            <th>Whoom to Meet</th>
                                            <th>IN Time</th>
                                            <th>OUT Time</th>                                            
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    @if($attdnc_sheet != null)
                                        @foreach($attdnc_sheet as $op)
                                        <tr>
                                            <td>{{ $op->visitor_name }}</td>
                                            <td>{{ $op->visitor_address }}</td>
                                            <td>{{ $op->visitor_email }}</td>
                                            <td>{{ $op->visitor_ph }}</td>
                                            <td>{{ $op->purpose }}</td>
                                            <td>{{ $op->whoom_to_meet }}</td>
                                            <td>{{ $op->in_time }}</td>
                                            <td>{{ $op->out_time }}</td>
                                            <td> 
                                            @if($op->checkout_status != 1)
                                                <button type="button" class="btn btn-info btn-rounded m-b-10 m-l-5" data-toggle="modal" data-target="#RoomRegModal{{$op->id}}"">Check-out</button> </li>
                                                @include('pages.op.checkouttimemodal',['id'=>$op->id])
                                            @else
                                            <button type="button" class="btn btn-default btn-rounded m-b-10 m-l-5" disabled>Check-out</button> </li>
                                               
                                            @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->





        </div>
</section>
@endsection
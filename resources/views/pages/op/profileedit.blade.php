@extends('layouts.op.opDashboard')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <!-- <h2>BLANK PAGE</h2> -->
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                    @if(session()->has('message.level'))
                        <div class="alert alert-{{ session('message.level') }}"> 
                        {!! session('message.content') !!}
                        </div>
                    @endif
                        <div class="header">
                            <h2>
                               Profile Edit
                            </h2>
                            <!-- <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul> -->
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    <form action="/op/profile/save" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="{{ $profile_edit_data['id'] }}">
                        <div class="body">
                            <!-- <h2 class="card-inside-title">With Icon</h2> -->
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">person</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="op_name" class="form-control email" value="{{ $profile_edit_data['name'] }}" placeholder="Employee Email-Id" >
                                        </div>
                                    
                                </div>
                                </div>                                
                            </div>
                            <div class="row clearfix">
                            <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">phone</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" maxlength="10" minlength="10" name="op_ph" value="{{ $profile_edit_data['ph'] }}" class="form-control email" placeholder="Phone#" >
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">photo</i>
                                        </span>
                                        <div class="form-line">
                                         <input type="file" name="op_img" class="form-control" value="{{ $profile_edit_data['img'] }}">                                         
                                        </div>
                                       
                                    </div>
                                </div>
                                <div class="col-md-4">
                                <img class="media-object" src="{{ asset('uploads/op/profile_pictures/') }}/{{ $profile_edit_data['img'] }}" width="50" height="50">
                                </div>
                            </div>
                        
                            
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">Update</button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>





        </div>
    </section>


@endsection
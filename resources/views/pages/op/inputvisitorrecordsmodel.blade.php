<div class="modal" id="VisitorRecord" 
     tabindex="-1" role="dialog" 
     aria-labelledby="favoritesModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" 
        id="favoritesModalLabel">Add Visitor's Records </h4>
        <button type="button" class="close" 
          data-dismiss="modal"
          aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
      </div>
   
      <div class="modal-body">
      @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    <form id="form" action="/op/attendence/save" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="body">
                            <!-- <h2 class="card-inside-title">With Icon</h2> -->
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">person</i>
                                        </span>
                                        <div class="form-line">
                                        <input type="text" name="visitor_name" class="form-control" placeholder="Visitor Name" required>
                                       
                                        <!-- <select class="form-control show-tick" name="cmp_name" data-live-search="true">
                                       
                                    </select> -->
                                    </div>
                                    
                                </div>
                                </div> 
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">home</i>
                                        </span>
                                        <div class="form-line">
                                        <input type="text" name="visitor_address" class="form-control" placeholder="Visitor Address" required>
                                       
                                        <!-- <select class="form-control show-tick" name="cmp_name" data-live-search="true">
                                       
                                    </select> -->
                                    </div>
                                    
                                </div>
                                </div>                                
                            </div>
                            <div class="row clearfix">
                            <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">email</i>
                                        </span>
                                        
                                        <div class="form-line">
                                        <input type="text" name="visitor_email" class="form-control" placeholder="Visitor Email" required>
                                      
                                 </div>
                                        
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">phone</i>
                                        </span>
                                        <div class="form-line">
                                        <input type="text" name="visitor_phn" class="form-control" placeholder="Visitor Phone No." required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-8">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">store_mall_directory</i>
                                        </span>
                                        <div class="form-line">
                                            <textarea type="text" name="purpose" rows="4" cols="35" maxlength="200" placeholder="Program details (200 characters left)" required></textarea>
                                        </div>
                                    </div>
                                </div>                               
                            </div>

                            <!-- <div class="row clearfix"> -->
                                <!-- <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">alarm_on</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="time" name="in_time" class="form-control" required>
                                            
                                        </div>
                                    </div>
                                </div> -->
                                <!-- <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">call_made</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="time" name="out_time" class="form-control" required>
                                        </div>
                                    </div>
                                </div> -->
                                <!-- <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">add_location</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="cmp_address" class="form-control" placeholder="Registered Address Line 1" required>
                                        </div>
                                    </div>
                                </div> -->
                               
                            <!-- </div> -->

                            
                            <div class="row clearfix">
                            <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">person</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="visitor_wtom" class="form-control" placeholder="Whom to meet" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">assignment</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="date" class="form-control date" value="{{ date('Y-m-d') }}" placeholder="Today's date" disabled>
                                            <input type="hidden" name="dt" value="{{ date('Y-m-d') }}">
                                        </div>
                                    </div>
                                </div>
                                
                                <!-- <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">add_location</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="cmp_pincode" class="form-control" placeholder="Pincode" required>
                                        </div>
                                    </div>
                                </div> -->
                               
                            </div>
                            
                            <button type="submit" id="submit" class="btn btn-primary m-t-15 waves-effect" onclick="disable()">Save</button>
                            </form>
                            <script>
                            function disable(){
                              document.getElementById('form').submit();
                              document.getElementById('submit').disabled = true;
                            }
                            </script>
      </div>
    </div>
  </div>
</div>
 <!-- For accepted -->
 <div class="modal fade" id="FeedbackModal1{{$id}}" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="largeModalLabel"></h4>
                        </div>
                        <div class="modal-body">
                           
                        <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  
                    <form action="/admin/booking/req" method="post" enctype="multipart/form-data">
                   
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="{{ $id }}">
                    <input type="hidden" name="status" value="1">
                        <div class="body">
                            <!-- <h2 class="card-inside-title">With Icon</h2> -->
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                        <h3> Are you sure? </h3>
                                        <!-- <div class="form-line">
                                            <textarea name="feedback" class="form-control date" placeholder="Give any feedback" required></textarea>
                                        </div> -->
                                    </div>
                                                          
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary waves-effect">Yes</button>
                            <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">No</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>



 <!-- For not accepted -->
 <div class="modal fade" id="FeedbackModal2{{$id}}" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="largeModalLabel"></h4>
                        </div>
                        <div class="modal-body">
                           
                        <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                
                    <form action="/admin/booking/req" method="post" enctype="multipart/form-data">
            
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="{{ $id }}">
                    <input type="hidden" name="status" value="2">
                        <div class="body">
                            <!-- <h2 class="card-inside-title">With Icon</h2> -->
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                        <h3> Are you sure? </h3>
                                        <!-- <div class="form-line">
                                            <textarea name="feedback" class="form-control date" placeholder="Give any feedback" required></textarea>
                                        </div> -->
                                    </div>
                                                          
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary waves-effect">Yes</button>
                            <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">No</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>


 <!-- For temporary accept -->
 <div class="modal fade" id="FeedbackModal3{{$id}}" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="largeModalLabel"></h4>
                        </div>
                        <div class="modal-body">
                           
                        <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                
                    <form action="/admin/booking/req" method="post" enctype="multipart/form-data">
            
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="{{ $id }}">
                    <input type="hidden" name="status" value="3">
                        <div class="body">
                            <!-- <h2 class="card-inside-title">With Icon</h2> -->
                            <div class="row clearfix">
                                <div class="col-md-12">
                                
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons"></i>
                                        </span>
                                        <h4> Are you sure? </h4>
                                        <!-- <div class="form-line">
                                            <textarea name="feedback" class="form-control date" placeholder="Give any feedback" required></textarea>
                                        </div> -->
                                    </div>
                                                          
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary waves-effect">Yes</button>
                            <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">No</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
 <!-- Large Size -->
 <div class="modal fade" id="DeleteModal{{$id}}" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <!-- <h4 class="modal-title" id="largeModalLabel">Edit Operator Information</h4> -->
                        </div>
                        
                        <div class="modal-body">
                        @if($activate_id != 0)
                             <center> <h4> Are you sure you want to deactivate {{ $name }} ?</h4> </center>
                        @else
                             <center> <h4> Are you sure you want to activate {{ $name }} ?</h4> </center>
                        @endif
                        
                         </div>
                        
                        <div class="modal-footer">
                        <center>
                        @if($activate_id != 0)
                            <a href="/admin/operator/deactive/{{$email}}">
                            <button type="button" class="btn btn-primary waves-effect">Yes</button></a>
                            <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">No</button></center>
                        @else
                            <a href="/admin/operator/active/{{$email}}">
                            <button type="button" class="btn btn-primary waves-effect">Yes</button></a>
                            <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">No</button></center>
                        @endif

                        </div>
                    </div>
                </div>
            </div>
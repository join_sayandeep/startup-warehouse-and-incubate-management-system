@extends('layouts.admin.adminDashboard')

@section('content')

<section class="content">
        <div class="container">
            <div class="block-header">
                <h2>Incubatee Not Accepted  List</h2>
            </div>
            <!-- Basic Example -->
            <!-- loop start -->
            
            @if($categorized_list != null)
                @foreach($categorized_list as $op)
            <!-- <div class="row "> -->
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card" style="height:400px;">
                        <div class="header bg-{{$class}}">
                        <h5>
                            {{$op->company_email}}
                            </h5>
                            <ul class="header-dropdown m-r--5">
                             <!-- <li>
                                    <a class="btn btn-primary btn-rounded" data-toggle="modal" data-target="#FeedbackRestoreModal{{$op->id}}">
                                        <i class="material-icons">settings_backup_restore</i>
                                    </a>
                                </li> -->
                               <!-- <li>
                                    <a href="/admin/room/req/1/{{$op->id}}">
                                        <i class="material-icons">done</i>
                                    </a>
                                </li> -->
                            </ul>
                        </div>
                        @include('pages.admin.feedbackrestoremodal',['id'=>$op->id])
                        <div class="body">
                            <strong>Date:</strong>   {{ $op->from_date }} <br>
                            <strong>Time:</strong>  
                            
                            @foreach($users as $slotid)
                                        @if($op->notify_id == $slotid->notify_id)
                                             
                                                    {{ $slotid->timings }}
                                               
                                        @endif
                                    @endforeach
                                     <br>
                            <strong>Request for:</strong> {{$op->room}} <br>
                            <strong>Purpose:</strong>  <span style="word-wrap: break-word;">  {{ $op->purpose }}     </span> 
                    	</div>
                    </div>
				<!-- </div> -->
            </div>
            <!-- #END# Basic Example -->
            @endforeach
            @endif
        </div>
    </section>



@endsection
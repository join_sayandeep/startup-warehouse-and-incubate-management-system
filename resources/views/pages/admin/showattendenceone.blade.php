@extends('layouts.admin.adminDashboard')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <!-- <h2>BLANK PAGE</h2> -->
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                    @if(session()->has('message.level'))
                        <div class="alert alert-{{ session('message.level') }}"> 
                        {!! session('message.content') !!}
                        </div>
                    @endif
                        <div class="header">
                            <h2>
                              Show Visitor's Records
                            </h2>
                            <!-- <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul> -->
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
        <script>
                      function datecheck(){

                        var fdt = document.getElementById('fdate').value;
                        var tdt = document.getElementById('tdate').value;

                         if(fdt != null && 0 != fdt.length && fdt != "0000-00-00"){
                            dt1 = new Date(fdt);
                            dt2 = new Date();
                            var diff = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
                          
                               if(tdt > fdt){
                                
                                    document.getElementById('submitbtn').disabled = false;
                               }
                               else{
                                document.getElementById('submitbtn').disabled = true;
                               }
                         }else{
                            document.getElementById('submitbtn').disabled = true;
                         }
                       
                    }
    </script>
                    <form action="/admin/visitors/list" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                   
                        <div class="body">
                            <!-- <h2 class="card-inside-title">With Icon</h2> -->
                            <div class="row clearfix">
                                <div class="col-md-5">
                                    <div class="input-group">
                                    <label>From</label>
                                        <span class="input-group-addon">
                                            <i class="material-icons">assignment</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="date" name="fdt" id="fdate" class="form-control date" required>
                                        </div>
                                    
                                </div>                              
                            </div>
                             <div class="col-md-5">
                                    <div class="input-group">
                                    <label>To</label>
                                        <span class="input-group-addon">
                                            <i class="material-icons">assignment</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="date" name="tdt" id="tdate" class="form-control date" required onchange="datecheck()">
                                        </div>
                                    
                                </div>                              
                            </div>
                            </div>
                            
                            <button type="submit" id="submitbtn" class="btn btn-primary m-t-15 waves-effect" disabled>Show</button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>





        </div>
    </section>


@endsection
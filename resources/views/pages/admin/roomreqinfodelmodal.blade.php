<div class="modal" id="RoomDelModal{{$id}}" 
     tabindex="-1" role="dialog" 
     aria-labelledby="favoritesModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" 
        id="favoritesModalLabel"> </h4>
        <button type="button" class="close" 
          data-dismiss="modal"
          aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
      </div>
   
      <div class="modal-body">
      <form action="/admin/bookedroom/delete" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="{{ $id }}">
                    @foreach($slot_list_id as $sid)
                    <input type="hidden" name="slot_id[]" value="{{ $sid }}">
                    @endforeach
                            <!-- <h2 class="card-inside-title">With Icon</h2> -->
                            
                            <div class="row clearfix">
                            <div class="col-md-10">
                                    <div class="input-group">
                                      <span> Are you sure? </span>
                                    </div>
                                </div>
                            </div>
                                  
      </div>
      <div class="modal-footer">
        
        <span class="pull-right">
        <button type="button" 
           class="btn btn-default" 
           data-dismiss="modal">No</button> &nbsp;
        <button type="submit" class="btn btn-primary">Yes</button>
        </span>
        </form>
      </div>
    </div>
  </div>
</div>
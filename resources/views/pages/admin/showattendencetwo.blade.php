@extends('layouts.admin.adminDashboard')

@section('content')
<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <!-- <h2>BLANK PAGE</h2> -->
            </div>

 <!-- Exportable Table -->
 <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                    @if(session()->has('message.level'))
                        <div class="alert alert-{{ session('message.level') }}"> 
                        {!! session('message.content') !!}
                        </div>
                    @endif
                        <div class="header">
                            <h2>
                              Visitor's Records
                            </h2>
                            <!-- <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul> -->
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Address</th>
                                            <th>Email-Id</th>
                                            <th>Phone</th>
                                            <th>Whoom to Meet</th>
                                            <th>Program Details</th>
                                            <th>IN Time</th>
                                            <th>OUT Time</th>                                            
                                            <!-- <th>Total Time</th> -->
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Address</th>
                                            <th>Email-Id</th>
                                            <th>Phone</th>
                                            <th>Whoom to Meet</th>
                                            <th>Program Details</th>
                                            <th>IN Time</th>
                                            <th>OUT Time</th>                                            
                                            <!-- <th>Total Time</th> -->
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    @if($attdnc_sheet != null)
                                        @foreach($attdnc_sheet as $op)
                                        <tr>
                                            <td>{{ $op->visitor_name }}</td>
                                            <td>{{ $op->visitor_address }}</td>
                                            <td>{{ $op->visitor_email }}</td>
                                            <td>{{ $op->visitor_ph }}</td>
                                            <td>{{ $op->whoom_to_meet }}</td>
                                            <td>{{ $op->purpose }}</td>
                                            <td>{{ $op->in_time }}</td>
                                            <td>{{ $op->out_time }}</td>
                                            <!-- <td>{{ $op->total_time }} Mins</td> -->
                                        </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->





        </div>
</section>
@endsection
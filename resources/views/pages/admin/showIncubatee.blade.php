@extends('layouts.admin.adminDashboard')

@section('content')
<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <!-- <h2>BLANK PAGE</h2> -->
            </div>

 <!-- Exportable Table -->
 <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                    @if(session()->has('message.level'))
                        <div class="alert alert-{{ session('message.level') }}"> 
                        {!! session('message.content') !!}
                        </div>
                    @endif
                        <div class="header">
                            <h2>
                               Incubatee List
                            </h2>
                            <!-- <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul> -->
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Logo</th>
                                            <th>Name</th>
                                            <th>Email#</th>
                                            <th>PAN#</th>
                                            <th>Reg#</th>
                                            <th>Ph#</th>
                                            <th>Founders</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Address</th>
                                            <th>State</th>
                                            <th>City</th>
                                            <th>Pincode</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Logo</th>
                                            <th>Name</th>
                                            <th>Email#</th>
                                            <th>PAN#</th>
                                            <th>Reg#</th>
                                            <th>Ph#</th>
                                            <th>Founders</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Address</th>
                                            <th>State</th>
                                            <th>City</th>
                                            <th>Pincode</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    @if($incubatee != null)
                                        @foreach($incubatee as $op)
                                        <tr> 
                                            <td><img height="50" width="50" src="{{ asset('/uploads/incubatee/profile_pictures/'.$op->company_logo) }}"></td>
                                            <td>{{ $op->company_name }}</td>
                                            <td>{{ $op->company_email }}</td>
                                            <td>{{ $op->pan_no }}</td>
                                            <td>{{ $op->company_reg_no }}</td>
                                            <td>{{ $op->primary_ph }}</td>
                                            <td>{{ $op->founders }}</td>
                                            <td>{{ $op->start_dt }}</td>
                                            <td>{{ $op->end_dt }}</td>
                                            <td>{{ $op->address }}</td>
                                            <td>{{ $op->state }}</td>
                                            <td>{{ $op->city }}</td>
                                            <td>{{ $op->pincode }}</td>
                                            <td>
                                            <!-- <button type="submit" class="btn btn-primary m-t-10 waves-effect" data-toggle="modal" data-target="#EditModal{{$op->id}}"><i class="material-icons">mode_edit</i></button><br> -->
                                            @if($op->account_status != 0)
                                            <button type="submit" class="btn btn-success m-t-10 waves-effect" data-toggle="modal" data-target="#DeleteModal{{$op->id}}"><i class="material-icons">account_circle</i></button> 
                                            @include('pages.admin.deactivateActivateIncubateeModal',['activate_id'=>'1','id' => $op->id,'name' => $op->company_name,'email' => $op->company_email])
                                            @else
                                            <button type="submit" class="btn btn-danger m-t-10 waves-effect" data-toggle="modal" data-target="#DeleteModal{{$op->id}}"><i class="material-icons">account_circle</i></button> 
                                            @include('pages.admin.deactivateActivateIncubateeModal',['activate_id'=>'0','id' => $op->id,'name' => $op->company_name,'email' => $op->company_email])
                                            @endif
                                        </td>
                                        </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->





        </div>
</section>
@endsection
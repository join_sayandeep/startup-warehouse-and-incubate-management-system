@extends('layouts.admin.adminDashboard')

@section('content')
<section class="content">
<div class="container-fluid">
           
            <div class="row card">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="body">

                    <form action="/admin/booking/save" method="post">  
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="cmp_email" value="{{ Session::get('adminsession') }}">
                            <input type="hidden" name="fdate" value="{{$fdt}}">
                            <input type="hidden" name="room" value="{{$room}}">
        
                            <div class="row">



@php
$tsarray=array('-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1');

    for($i=0;$i<=23;$i++)
{
    foreach($booked_tym as $bt)
 {
    if($i==($bt->time-1))
    {
        $tsarray[$bt->time-1]=1;
    }
    else
    {   
        if($tsarray[$i]!=1)
        $tsarray[$i]=0;

    }
    
}

 }  

foreach($timings as $tym)


if($tsarray[$tym->id-1] == 0 || $tsarray[$tym->id-1] == -1)

{


@endphp
                                            <div class="col-md-2">
                                                <div class="input-group input-group-lg">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox" class="filled-in" name="tym[]" value="{{$tym->id}}" id="ig_checkbox{{$tym->id}}">
                                                        <label for="ig_checkbox{{$tym->id}}">{{$tym->timings}}</label>
                                                    </span>
                                                </div>
                                            </div>

@php
}
else
{
@endphp
                                        <div class="col-md-2">
                                            <div class="input-group input-group-lg">
                                                <span class="input-group-addon">
                                                    <input type="checkbox" class="filled-in" name="tym[]" value="{{$tym->id}}" id="ig_checkbox{{$tym->id}}" checked disabled>
                                                    <label for="ig_checkbox{{$tym->id}}">{{$tym->timings}}</label>
                                                </span>
                                            </div>
                                        </div>



@php
}

@endphp


                            <div class="col-lg-12 col-md-3 col-sm-3 col-xs-2">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">question_answer</i>
                                        </span>
                                        <div class="form-line">
                                            <textarea name="purpose" rows="3" cols="30" maxlength="250" placeholder="Describe your needs (250 characters left)" required>{{ Session::get('purpose') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                         
                            </div>
                            <button type="submit" id="submitbtn" class="btn btn-primary m-t-15 waves-effect">Next</button>
                               </form>


                   
                    </div>
                   </div>
                </div>  
                               
</div>
    
</section>


@endsection
 <!-- Large Size -->
            <div class="modal fade" id="EditModal{{$id}}" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="largeModalLabel">Edit Operator Information</h4>
                        </div>
                        <div class="modal-body">
                           
                        <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <form action="/admin/operator/update" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="body">
                            <!-- <h2 class="card-inside-title">With Icon</h2> -->
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">email</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="email" name="op_email" class="form-control date" placeholder="Email Id" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">person</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="op_name" class="form-control date" placeholder="Name" >
                                        </div>
                                        
                                    </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">phone</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="op_phn" class="form-control" placeholder="Phone" maxlength="10" minlength="10" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">add_a_photo</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="file" name="op_profile_pic" class="form-control">
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary waves-effect">SAVE CHANGES</button>
                            <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">CLOSE</button>
                        </div>
                    </div>
                </div>
            </div>
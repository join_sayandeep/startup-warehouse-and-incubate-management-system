<div class="modal" id="RoomRegModal{{$id}}" 
     tabindex="-1" role="dialog" 
     aria-labelledby="favoritesModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" 
        id="favoritesModalLabel"> </h4>
        <button type="button" class="close" 
          data-dismiss="modal"
          aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
      </div>
   
      <div class="modal-body">
      <form action="/admin/hallbook/update" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <!-- <h2 class="card-inside-title">With Icon</h2> -->
                            <div class="row clearfix">
                            <div class="col-md-6">
                                    <div class="input-group">
                                    <label>Date</label>
                                        <span class="input-group-addon">
                                            <i class="material-icons">date_range</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="date"  class="form-control date" value="{{ $dt }}" required disabled>
                                            <input type="hidden" name="fdate" value="{{ $dt }}">
                                            <input type="hidden" name="id" value="{{ $id }}">
                                            <input type="hidden" name="notify_id" value="{{ $notify_id }}">
                                           
                                            @foreach($slot_list_id as $slt_id)
                                             <input type="hidden" name="slt_id[]" value="{{ $slt_id }}">
                                            @endforeach
                                        </div>
                                    </div>
                                </div>             
                                <div class="col-md-6">
                                
                                    <div class="input-group">
                                    <input type="hidden" name="cmp_email" value="{{ Session::get('incubateesession') }}">
                                    <span class="input-group-addon">
                                            <i class="material-icons">domain</i>
                                        </span>
                                        <div class="form-line">
                                        <input type="hidden" name="room" value="{{ $room }}">
                                        <select class="form-control show-tick"  value="{{ $room }}" data-live-search="true" disabled>
                                        <option>Hall</option>
                                        <option>Conference Hall</option>
                                         
                                    </select>
                                     </div>
                                    </div>
                            
                                    <!-- <div class="input-group">
                                    <label>To</label>
                                        <span class="input-group-addon">
                                            <i class="material-icons">date_range</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="date" name="tdate" class="form-control date" value="" required>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                            <div class="row clearfix">
                            
@php
$tsarray=array('-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1','-1');

    for($i=0;$i<=23;$i++)
{
    foreach($transaction_id as $bt)
 {
    if($i==($bt-1))
    {
        $tsarray[$bt-1]=1;
    }
    else
    {   
        if($tsarray[$i]!=1)
        $tsarray[$i]=0;

    }
    
}

 }  

 for($i=0;$i<=23;$i++)
{
    foreach($booked_id as $bid)
    {
        if($i == ($bid-1))
           {
               $tsarray[$bid-1] = 2;
           }
    }
}

foreach($slot_id as $tym)


if($tsarray[$tym->id-1] == 0 || $tsarray[$tym->id-1] == -1)

{


@endphp
                                            <div class="col-md-2">
                                                <div class="input-group input-group-lg">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox" class="filled-in" name="tym[]" value="{{$tym->id}}" id="ig_checkbox{{$tym->id}}{{$id}}">
                                                        <label for="ig_checkbox{{$tym->id}}{{$id}}">{{$tym->timings}}</label>
                                                    </span>
                                                </div>
                                            </div>

@php
}
else if($tsarray[$tym->id-1] == 2)
{
@endphp
                                        <div class="col-md-2">
                                            <div class="input-group input-group-lg">
                                                <span class="input-group-addon">
                                                    <input type="checkbox" class="filled-in" name="tym[]" value="{{$tym->id}}" id="ig_checkbox{{$tym->id}}{{$id}}" checked >
                                                    <label for="ig_checkbox{{$tym->id}}{{$id}}">{{$tym->timings}}</label>
                                                </span>
                                            </div>
                                        </div>



@php
}else{
@endphp
<div class="col-md-2">
                                            <div class="input-group input-group-lg">
                                                <span class="input-group-addon">
                                                    <input type="checkbox" class="filled-in" name="tym[]" value="{{$tym->id}}" id="ig_checkbox{{$tym->id}}{{$id}}" checked disabled>
                                                    <label for="ig_checkbox{{$tym->id}}{{$id}}">{{$tym->timings}}</label>
                                                </span>
                                            </div>
                                        </div>
@php
}

@endphp
                            </div>
                            <div class="row clearfix">
                            
                            <!-- <div class="col-md-5"> -->
                                    <!-- <div class="input-group"> -->
                                        <!-- <span class="input-group-addon"> -->
                                            <!-- <i class="material-icons">email</i> -->
                                        <!-- </span> -->
                                        <!-- <div class="form-line"> -->
                                            <!-- <input type="email" value="{{ Session::get('incubateesession') }}" class="form-control date" placeholder="Company Email" disabled> -->
                                          
                                        <!-- </div> -->
                                    <!-- </div> -->
                                <!-- </div> -->
                               
                            </div>
                            <div class="row clearfix">
                            <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">question_answer</i>
                                        </span>
                                        <div class="form-line">
                                            <textarea name="purpose" rows="3" cols="60" placeholder="Describe your needs" required>{{ $purpose }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                  
      </div>
      <div class="modal-footer">
        <!-- <button type="button" 
           class="btn btn-default" 
           data-dismiss="modal"></button> -->
        <span class="pull-right">
        <button type="submit" class="btn btn-primary">Save</button>
        </span>
        </form>
      </div>
    </div>
  </div>
</div>
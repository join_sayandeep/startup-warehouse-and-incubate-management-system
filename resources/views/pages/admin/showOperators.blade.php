@extends('layouts.admin.adminDashboard')

@section('content')
<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <!-- <h2>BLANK PAGE</h2> -->
            </div>

 <!-- Exportable Table -->
 <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                    @if(session()->has('message.level'))
                        <div class="alert alert-{{ session('message.level') }}"> 
                        {!! session('message.content') !!}
                        </div>
                    @endif
                        <div class="header">
                            <h2>
                               Operators List
                            </h2>
                            <!-- <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul> -->
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Profile Img</th>
                                            <th>Email</th>
                                            <th>Name</th>
                                            <th>Phone#</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            
                                            <th>Profile Img</th>
                                            <th>Email</th>
                                            <th>Name</th>
                                            <th>Phone#</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    @if($operators != null)
                                        @foreach($operators as $op)
                                        <tr>
                                            <td><center><img height="50" width="50" src="{{ asset('/uploads/op/profile_pictures/'.$op->img) }}"></center></td>
                                            <td>{{ $op->email }}</td>
                                            <td>{{ $op->name }}</td>
                                            <td>{{ $op->ph }}</td>
                                            <td>
                                            <!-- <button type="submit" class="btn btn-primary m-t-10 waves-effect" data-toggle="modal" data-target="#EditModal{{$op->id}}"><i class="material-icons">mode_edit</i></button><br> -->
                                            @if($op->account_status != 0)
                                            <button type="submit" class="btn btn-success m-t-10 waves-effect" data-toggle="modal" data-target="#DeleteModal{{$op->id}}"><i class="material-icons">account_circle</i></button> 
                                            @include('pages.admin.deactivateActivateOperatorModal',['activate_id'=>'1','id' => $op->id,'name' => $op->name,'email' => $op->email])
                                            @else
                                            <button type="submit" class="btn btn-danger m-t-10 waves-effect" data-toggle="modal" data-target="#DeleteModal{{$op->id}}"><i class="material-icons">account_circle</i></button> 
                                            @include('pages.admin.deactivateActivateOperatorModal',['activate_id'=>'0','id' => $op->id,'name' => $op->name,'email' => $op->email])
                                            @endif
                                        </td>
                                        </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->





        </div>
</section>
@endsection
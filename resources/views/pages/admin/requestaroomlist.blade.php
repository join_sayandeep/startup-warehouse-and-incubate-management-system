@extends('layouts.admin.adminDashboard')

@section('content')

<section class="content">
        <div class="container">
            <div class="block-header">
                <h2>Incubatee Bookings List</h2>
            </div>
            <!-- Basic Example -->
            <!-- loop start -->
            
            @if($req_room_list != null)
                @foreach($req_room_list as $op)
            <!-- <div class="row "> -->
                @if($op->req_status == '0')
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" >
                    <div class="card" style="height:400px;">
                        <div class="header bg-blue">
                            <h5>
                            {{$op->company_email}}
                            </h5>
                            <ul class="header-dropdown m-r--5">
                                <li>
                                    <a class="btn btn-danger btn-rounded"  data-toggle="modal" data-target="#FeedbackModal2{{$op->id}}">
                                        <i data-toggle="tooltip" title="Not  Accept"  class="material-icons">clear</i>
                                    </a>
                                    
                                </li>
                                <li>
                                    <a class="btn btn-success btn-rounded"  data-toggle="modal" data-target="#FeedbackModal1{{$op->id}}">
                                        <i data-toggle="tooltip" title="Accept"  class="material-icons">done</i>
                                           
                                    </a>
                                </li>

                                 <li>
                                    <a class="btn btn-warning btn-rounded" data-toggle="modal" data-target="#FeedbackModal3{{$op->id}}">
                                        <i data-toggle="tooltip" title="Temporary Accept" class="material-icons">query_builder</i>
                                           
                                    </a>
                                </li>
                            </ul>
                        </div>
                        @include('pages.admin.feedbackmodal',['id'=>$op->id])
                        @include('pages.admin.feedbackmodal',['id'=>$op->id])
                        @include('pages.admin.feedbackmodal',['id'=>$op->id])
                        <div class="body">
                            <strong>Date:</strong>   {{ $op->from_date }} <br>
                            <strong>Time:</strong> 
                            
                                   @foreach($users as $slotid)
                                        @if($op->notify_id == $slotid->notify_id)
                                             
                                                    {{ $slotid->timings }}
                                               
                                        @endif
                                    @endforeach
                             <br>
                            <strong>Request for:</strong> {{$op->room}} <br>
                            <strong>Purpose</strong> <span style="word-wrap: break-word;">  {{ $op->purpose }}     </span> 
                    	</div>
                    </div>
				<!-- </div> -->
            </div>
            <!-- #END# Basic Example -->
            @endif
            @endforeach
            @endif
        </div>
    </section>
    <script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>


@endsection
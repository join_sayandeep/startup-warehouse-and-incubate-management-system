 <!-- For accepted -->
 <div class="modal fade" id="FeedbackRestoreModal{{$id}}" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="largeModalLabel">Give feedback</h4>
                        </div>
                        <div class="modal-body">
                           
                        <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  
                    <form action="/admin/restore/req" method="post" enctype="multipart/form-data">
                   
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="{{ $id }}">
                    <input type="hidden" name="status" value="1">
                        <div class="body">
                            <!-- <h2 class="card-inside-title">With Icon</h2> -->
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">question_answer</i>
                                        </span>
                                        <div class="form-line">
                                            <textarea name="feedback" class="form-control date" placeholder="Give any feedback" required></textarea>
                                        </div>
                                    </div>
                                                          
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary waves-effect">DONE</button>
                            <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">CLOSE</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>

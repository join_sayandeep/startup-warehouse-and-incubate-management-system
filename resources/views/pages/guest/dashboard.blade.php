@extends('layouts.guest.GuestDashboard')

@section('content')
    <section class="content">
        <div class="container-fluid">
        <div class="block-header">
                <h2>Dashboard</h2>
            </div>
            <div class="row">
                <!-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box-4 hover-zoom-effect">
                        <div class="icon">
                            <i class="material-icons col-pink">person</i>
                        </div>
                        <div class="content">
                            <div class="text">Operators</div>
                            <div class="number">22</div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box-4 hover-zoom-effect">
                        <div class="icon">
                            <i class="material-icons col-blue">person</i>
                        </div>
                        <div class="content">
                            <div class="text">Incubatees</div>
                            <div class="number">22</div>
                        </div>
                    </div>
                </div> -->
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box-4 hover-zoom-effect">
                        <div class="icon">
                            <i class="material-icons col-red">person</i>
                        </div>
                        <div class="content">
                            <div class="text">Incubatees</div>
                            <div class="number">{{$no_of_incubatee}}</div>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box-4 hover-zoom-effect">
                        <div class="icon">
                            <i class="material-icons col-cyan">add_location</i>
                        </div>
                        <div class="content">
                            <div class="text">Accepted Bookings</div>
                            <div class="number">21</div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </section>

 
@endsection
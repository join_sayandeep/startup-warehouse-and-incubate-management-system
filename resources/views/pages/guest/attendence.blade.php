@extends('layouts.guest.GuestDashboard')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <!-- <h2>BLANK PAGE</h2> -->
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                    @if(session()->has('message.level'))
                        <div class="alert alert-{{ session('message.level') }}"> 
                        {!! session('message.content') !!}
                        </div>
                    @endif
                        <div class="header">
                            <h2>
                                Visitors Record book
                            </h2>
                            <!-- <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul> -->
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    <form action="/guest/attendence/save" id="form" onsubmit="submit.disabled = true; return true;" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="body">
                            <!-- <h2 class="card-inside-title">With Icon</h2> -->
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">person</i>
                                        </span>
                                        <div class="form-line">
                                        <input type="text" name="visitor_name" class="form-control" placeholder="Visitor Name" required>
                                       
                                        <!-- <select class="form-control show-tick" name="cmp_name" data-live-search="true">
                                       
                                    </select> -->
                                    </div>
                                    
                                </div>
                                </div> 
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">home</i>
                                        </span>
                                        <div class="form-line">
                                        <input type="text" name="visitor_address" class="form-control" placeholder="Visitor Address" required>
                                       
                                        <!-- <select class="form-control show-tick" name="cmp_name" data-live-search="true">
                                       
                                    </select> -->
                                    </div>
                                    
                                </div>
                                </div>                                
                            </div>
                            <div class="row clearfix">
                            <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">email</i>
                                        </span>
                                        
                                        <div class="form-line">
                                        <input type="text" name="visitor_email" class="form-control" placeholder="Visitor Email" required>
                                        <!-- <select class="form-control show-tick" name="emp_email" data-live-search="true">
                                        @if($attendence_list != null)
                                           @foreach($attendence_list as $incub)
                                        <option>{{ $incub->emp_email }}</option>
                                           @endforeach
                                        @endif
                                    </select>  -->
                                 </div>
                                        
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">phone</i>
                                        </span>
                                        <div class="form-line">
                                        <input type="text" name="visitor_phn" class="form-control" placeholder="Visitor Phone No." required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-8">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">store_mall_directory</i>
                                        </span>
                                        <div class="form-line">
                                            <textarea type="text" name="purpose" rows="4" cols="90" maxlength="200" placeholder="Program details (200 characters left)" required></textarea>
                                        </div>
                                    </div>
                                </div>                               
                            </div>

                            <!-- <div class="row clearfix"> -->
                                <!-- <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">alarm_on</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="time" name="in_time" class="form-control" required>
                                            
                                        </div>
                                    </div>
                                </div> -->
                                <!-- <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">call_made</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="time" name="out_time" class="form-control" required>
                                        </div>
                                    </div>
                                </div> -->
                                <!-- <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">add_location</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="cmp_address" class="form-control" placeholder="Registered Address Line 1" required>
                                        </div>
                                    </div>
                                </div> -->
                               
                            <!-- </div> -->

                            
                            <div class="row clearfix">
                            <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">person</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="visitor_wtom" class="form-control" placeholder="Whom to meet" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">assignment</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="date" class="form-control date" value="{{ date('Y-m-d') }}" placeholder="Today's date" disabled>
                                            <input type="hidden" name="dt" value="{{ date('Y-m-d') }}">
                                        </div>
                                    </div>
                                </div>
                                
                                <!-- <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">add_location</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="cmp_pincode" class="form-control" placeholder="Pincode" required>
                                        </div>
                                    </div>
                                </div> -->
                               
                            </div>
                            
                            <button type="submit" name="submit" class="btn btn-primary m-t-15 waves-effect">Save</button>
                            </form>
                           
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
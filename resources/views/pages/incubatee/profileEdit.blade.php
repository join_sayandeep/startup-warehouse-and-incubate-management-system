@extends('layouts.incubatee.incubateeDashboard')

@section('content')
<section class="content"> 
        <div class="container-fluid">
            <div class="block-header">
                <!-- <h2>BLANK PAGE</h2> -->
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                    @if(session()->has('message.level'))
                        <div class="alert alert-{{ session('message.level') }}"> 
                        {!! session('message.content') !!}
                        </div>
                    @endif
                        <div class="header">
                            <h2>
                                Edit Incubatee's Information
                            </h2>
                            <!-- <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul> -->
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    <form action="/incubatee/profile/update" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="{{ $incubatee_profile['id'] }}">
                        <div class="body">
                            <!-- <h2 class="card-inside-title">With Icon</h2> -->
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">account_balance</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="cmp_name" class="form-control date" value="{{$incubatee_profile['company_name']}}" placeholder="Company Name(Registered Name)" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">email</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="cmp_email" class="form-control date" value="{{$incubatee_profile['company_email']}}" placeholder="Company Email-Id" >
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">picture_in_picture</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="cmp_pan" class="form-control date" value="{{$incubatee_profile['pan_no']}}" placeholder="Company PAN#"  maxlength="10" minlength="10" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">store_mall_directory</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="cmp_reg_no" class="form-control" value="{{$incubatee_profile['company_reg_no']}}" placeholder="Company Registration No." required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">phone</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="cmp_ph" class="form-control" value="{{$incubatee_profile['primary_ph']}}" placeholder="Company Phone No." maxlength="10" minlength="10" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                            <!-- <i class="material-icons">add_a_photo</i> -->
                                        </span>
                                        <img class="media-object" src="{{ asset('uploads/incubatee/profile_pictures/') }}/{{$incubatee_profile['company_logo']}}" width="50" height="50">
                                  
                                    </div>
                                </div>
                               
                            </div>

                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">group</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="cmp_founders" class="form-control" value="{{$incubatee_profile['founders']}}" placeholder="Company Founders (e.g: xyz/Abc)" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">add_location</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="cmp_address" class="form-control" value="{{$incubatee_profile['address']}}" placeholder="Registered Address Line 1" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">add_a_photo</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="file" name="cmp_logo" class="form-control">
                                        </div>
                                    </div>
                                </div>
                               
                            </div>

                            
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">add_location</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="cmp_state" class="form-control" value="{{$incubatee_profile['state']}}" placeholder="State" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">add_location</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="cmp_city" class="form-control" value="{{$incubatee_profile['city']}}" placeholder="City" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">add_location</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="cmp_pincode" class="form-control" value="{{$incubatee_profile['pincode']}}" placeholder="Pincode" required>
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
                            
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">Update</button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
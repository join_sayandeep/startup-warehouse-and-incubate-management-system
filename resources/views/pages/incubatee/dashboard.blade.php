@extends('layouts.incubatee.incubateeDashboard')
 
@section('content')
    <section class="content">
        <div class="container-fluid">
        <div class="block-header">
                <h2>Dashboard</h2>
            </div>
            <div class="row">
                <!-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box-4 hover-zoom-effect">
                        <div class="icon">
                            <i class="material-icons col-pink">person</i>
                        </div>
                        <div class="content">
                            <div class="text">Operators</div>
                            <div class="number">32</div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box-4 hover-zoom-effect">
                        <div class="icon">
                            <i class="material-icons col-blue">person</i>
                        </div>
                        <div class="content">
                            <div class="text">Incubatees</div>
                            <div class="number">34</div>
                        </div>
                    </div>
                </div> -->
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box-4 hover-zoom-effect">
                        <div class="icon">
                        <i class="fa fa-exclamation-triangle col-blue" aria-hidden="true" style="font-size:65px;"></i>
                        </div>
                        <div class="content">
                            <div class="text">Pending Bookings</div>
                            <div class="number">{{$pending_bookings}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box-4 hover-zoom-effect">
                        <div class="icon">
                        <i class="fa fa-calendar-check-o col-green" aria-hidden="true" style="font-size:65px;"></i>
                        </div>
                        <div class="content">
                            <div class="text">Accepted Bookings</div>
                            <div class="number">{{$accepted_bookings}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box-4 hover-zoom-effect">
                        <div class="icon">
                        <i class="fa fa-calendar-check-o col-yellow" aria-hidden="true" style="font-size:65px;"></i>
                        </div>
                        <div class="content">
                            <div class="text">Temporary Accepted Bookings</div>
                            <div class="number">{{$temp_accepted_bookings}}</div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </section>

 
@endsection
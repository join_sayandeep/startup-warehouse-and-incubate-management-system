@extends('layouts.incubatee.incubateeDashboard')

@section('content')
<section class="content">
<div class="container-fluid">
            <div class="block-header">
                <!-- <h2>BLANK PAGE</h2> -->
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                    @if(session()->has('message.level'))
                        <div class="alert alert-{{ session('message.level') }}"> 
                        {!! session('message.content') !!}
                        </div>
                    @endif
                        <div class="header">
                            <h2>
                                Profile
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="/incubatee/profile/edit">Edit</a></li>
                                        <li><a href="/incubatee/profile/password">Change Password</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="body">
                            <!-- <h2 class="card-inside-title">With Icon</h2> -->
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">account_balance</i>
                                        </span>
                                        
                                            <label  class="form-control" >{{$incubatee_profile['company_name']}}</label>
                                       
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">email</i>
                                        </span>
                                        <label  class="form-control" >{{$incubatee_profile['company_email']}}</label>
                                        
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">picture_in_picture</i>
                                        </span>
                                        <label  class="form-control" >{{$incubatee_profile['pan_no']}}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">store_mall_directory</i>
                                        </span>
                                        <label  class="form-control" >{{$incubatee_profile['company_reg_no']}}</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">phone</i>
                                        </span>
                                        <label  class="form-control" >{{$incubatee_profile['primary_ph']}}</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">add_a_photo</i>
                                        </span>
                                        <img class="media-object" src="{{ asset('uploads/incubatee/profile_pictures/') }}/{{$incubatee_profile['company_logo']}}" width="50" height="50">
                                  
                                    </div>
                                </div>
                               
                            </div>

                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">group</i>
                                        </span>
                                        <label  class="form-control" >{{$incubatee_profile['founders']}}</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">add_location</i>
                                        </span>
                                        <label  class="form-control" >{{$incubatee_profile['address']}}</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                   
                                </div>
                               
                            </div>

                            
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">add_location</i>
                                        </span>
                                        <label  class="form-control" >{{$incubatee_profile['state']}}</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">add_location</i>
                                        </span>
                                        <label  class="form-control" >{{$incubatee_profile['city']}}</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">add_location</i>
                                        </span>
                                        <label  class="form-control" >{{$incubatee_profile['pincode']}}</label>
                                    </div>
                                </div>
                               
                            </div>
                            
                            <!-- <button type="submit" class="btn btn-primary m-t-15 waves-effect">Save</button> -->
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
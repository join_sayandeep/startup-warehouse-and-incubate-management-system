@extends('layouts.op.opDashboard')

@section('content')
<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                 <!-- <h2>BLANK PAGE</h2> -->
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                    @if(session()->has('message.level'))
                        <div class="alert alert-{{ session('message.level') }}"> 
                        {!! session('message.content') !!}
                        </div>
                    @endif
                        <div class="header">
                            <h2>
                               Book your Hall (Small/Big Conference Hall)
                            </h2>
                            <!-- <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul> -->
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    <script>
                    function singledatefunc(){
                     
                        document.getElementById('singledt').style.display = "block";
                        document.getElementById('rangedt').style.display = "none";
                        document.getElementById('singledate').disabled = false;
                        

                    }
                    function rangedate(){
                        
                        document.getElementById('singledt').style.display = "none";
                        document.getElementById('rangedt').style.display = "block";
                        document.getElementById('fdate').disabled = false;
                        document.getElementById('tdate').disabled = false;


                    }
                    function datecheck(){
                        var fdt = document.getElementById('fdate').value;
                        var tdt = document.getElementById('tdate').value;
                         if(fdt != null && 0 != fdt.length && fdt != "0000-00-00"){
                            dt1 = new Date(fdt);
                            dt2 = new Date();
                            var diff = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
                          
                               if(tdt > fdt && diff < 0){
                                
                                    document.getElementById('submitbtn').disabled = false;
                               }
                               else{
                                document.getElementById('submitbtn').disabled = true;
                               }
                         }else{
                            document.getElementById('submitbtn').disabled = true;
                         }
                       
                    }
                    function sdatecheck(){
                        var sdt = document.getElementById('singledate').value;
                        if(sdt != null && 0 != sdt.length && sdt != "0000-00-00"){
                            dt1 = new Date(sdt);
                            dt2 = new Date();
                            var diff = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
                          
                               if(diff < 0){
                                
                                    document.getElementById('submitbtn').disabled = false;
                               }
                               else{
                                document.getElementById('submitbtn').disabled = true;
                               }
                         }else{
                            document.getElementById('submitbtn').disabled = true;
                         }
                    }
                    function timecheck(){
                        var fdt = document.getElementById('from_time').value;
                        var tdt = document.getElementById('to_time').value;
                         if(fdt != null && 0 != fdt.length && fdt != "00:00"){
                            // dt1 = new Date(fdt);
                            // dt2 = new Date();
                            // var diff = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
                          
                               if(tdt > fdt){
                                
                                    document.getElementById('submitbtn').disabled = false;
                               }
                               else{
                                document.getElementById('submitbtn').disabled = true;
                               }
                         }else{
                            document.getElementById('submitbtn').disabled = true;
                         }
                    }
                    </script>
                    <form action="/op/request/save/check" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="body">
                            <!-- <h2 class="card-inside-title">With Icon</h2> -->
                            <!-- <div class="row clearfix">
                            <div class="col-md-4">
                            <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="radio" class="with-gap" name="datecat" value="1" id="ig_radio" onclick="singledatefunc()" required>
                                            <label for="ig_radio">Single Date</label>
                                        </span>
                                    </div>
                            </div>
                            
                            
                            <div class="col-md-4">
                            <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="radio" class="with-gap" name="datecat" value="2" id="ig_radio1" onclick="rangedate()" required>
                                            <label for="ig_radio1">Range Date</label>
                                        </span>
                                        
                                    </div>
                            </div>
                            </div> -->
                            <div class="row clearfix" id="rangedt" style="display:none;">
                            <div class="col-md-6">
                                    <div class="input-group">
                                    <label>From</label>
                                        <span class="input-group-addon">
                                            <i class="material-icons">date_range</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="date" id="fdate" name="fdate" value="{{ Session::get('fdate') }}" class="form-control date" disabled>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="col-md-6">
                                    <div class="input-group">
                                    <label>To</label>
                                        <span class="input-group-addon">
                                            <i class="material-icons">date_range</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="date" id="tdate" name="tdate" value="{{ Session::get('tdate') }}" class="form-control date" disabled onchange="datecheck()">
                                        </div>
                                    </div>
                                </div>
                            
                                </div>

                                <div class="row clearfix" id="singledt" style="display:block;">
                            <div class="col-md-6">
                                    <div class="input-group">
                                    <!-- <label>Date</label> -->
                                        <span class="input-group-addon">
                                            <i class="material-icons">date_range</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="date" id="singledate" name="fdate" value="{{ Session::get('fdate') }}" class="form-control date" onchange="sdatecheck()">
                                        </div>
                                    </div>
                                </div>
                            
                                <!-- <div class="col-md-6">
                                    <div class="input-group">
                                    <label>To</label>
                                        <span class="input-group-addon">
                                            <i class="material-icons">date_range</i>
                                        </span>
                                        <div class="form-line"> -->
                                            <!-- <input type="hidden" id="singledate" name="tdate" class="form-control date" value="0000-00-00" disabled> -->
                                        <!-- </div>
                                    </div>
                                </div>
                             -->
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">mail</i>
                                        </span>
                                        <div class="form-line">
                                        <input type="email" class="form-control" name="incubatee_email" placeholder="Primary Email-Id" required>
                                     </div>
                                    </div>
                                     </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">account_circle</i>
                                        </span>
                                        <div class="form-line">
                                        <input type="text" class="form-control" name="incubatee_name" placeholder="Name" required>
                                     </div>
                                    </div>
                                     </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">call</i>
                                        </span>
                                        <div class="form-line">
                                        <input type="number" class="form-control" name="incubatee_ph" placeholder="Phone #" required>
                                     </div>
                                    </div>
                                     </div>
                                </div>
                            <div class="row clearfix">
                            <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">domain</i>
                                        </span>
                                        <div class="form-line">
                                        <select class="form-control show-tick" name="room" value="{{ Session::get('room') }}" data-live-search="true" required>
                                        <option>Small Conference Hall</option>
                                        <option>Big Conference Hall</option>
                                         
                                    </select>
                                     </div>
                                    </div>
                                </div>
                                </div>

                            <div class="row clearfix">
                            
                            <div class="col-md-5">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <!-- <i class="material-icons">email</i> -->
                                        </span>
                                        <!-- <div class="form-line"> -->
                                            <!-- <input type="email" value="{{ Session::get('incubateesession') }}" class="form-control date" placeholder="Company Email" disabled> -->
                                           <input type="hidden" name="cmp_email" value="{{ Session::get('opsession') }}">
                                        <!-- </div> -->
                                    </div>
                                </div>
                                
                               
                            </div>
                            <!-- <div class="row clearfix">
                           
                            <div class="col-md-4">
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                            <i class="material-icons">question_answer</i>
                                        </span>
                                        <div class="form-line">
                                            <textarea name="purpose" rows="3" cols="60" maxlength="250" placeholder="Describe your needs (250 characters left)" required>{{ Session::get('purpose') }}</textarea>
                                        </div>
                                    </div>
                                </div>

                            </div> -->
                            <button type="submit" id="submitbtn" class="btn btn-primary m-t-15 waves-effect" disabled>Next</button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
@extends('layouts.op.opDashboard')

@section('content')
<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <!-- <h2>BLANK PAGE</h2> -->
            </div>

 <!-- Exportable Table -->
 <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                    @if(session()->has('message.level'))
                        <div class="alert alert-{{ session('message.level') }}"> 
                        {!! session('message.content') !!}
                        </div>
                    @endif
                        <div class="header">
                            <h2>
                               Booking List
                            </h2>
                            <!-- <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul> -->
                        </div>
                        <div class="body">
                  
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Incubatee</th>                                            
                                            <th>Ph#</th>
                                            <th>Timing</th>
                                            <th>Room</th>
                                            <th>Program Details</th>
                                            <!-- <th>Feedback(Restore)</th>                                             -->
                                            <th>status</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Date</th>
                                            <th>Incubatee</th>
                                            <th>Ph#</th>
                                            <th>Timing</th>
                                            <th>Room</th>
                                            <th>Program Details</th>
                                            <!-- <th>Feedback(Restore)</th> -->
                                            <th>status</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    @if($req_list != null)
                                        @foreach($req_list as $op)
                                        @php $transaction_id = array();
                                             $booked_id = array();
                                             $slot_list_id = array();
                                             $notify_id = array();
                                         @endphp
                                        <tr>
                                            <td>{{ $op->from_date }}</td>
                                            <td>{{$op->company_name}} <br> ({{$op->company_email}})</td>
                                            <td>{{$op->company_ph}}</td>
                                            <td> @foreach($slot_list as $sl)
                                            @php $transaction_id[] = $sl->time;                                            
                                                @endphp
                                               @if($op->notify_id == $sl->notify_id)
                                                    @php $notify_id = $op->notify_id;
                                                    $slot_list_id[] = $sl->id; @endphp
                                                      @foreach($slot_id as $sid)  
                                                        @if($sid->id == $sl->time)
                                                                {{ $sid->timings }} &nbsp;
                                                                @php $booked_id[] = $sid->id @endphp
                                                            @endif
                                                      @endforeach
                                               @endif
                                            @endforeach</td>
                                            <td>{{ $op->room }}</td>
                                            <td>{{ $op->purpose }}</td>
                                            <!-- <td>{{ $op->feedback }}</td> -->
                                            <!-- <td>{{ $op->restore_feedback }}</td> -->
                                            <td>@if($op->req_status != 0)
                                            <span class="badge bg-green">Accepted</span>
                                            @else
                                            <span class="badge bg-red">Not Accepted</span>
                                            @endif
                                            </td>
                                            <td>
                                            @if($op->req_status != 1)
                                            <button type="button" class="btn btn-warning btn-rounded m-b-10 m-l-5" disabled>Edit</button> </li>
                                            
                                            @else
                                            <button type="button" class="btn btn-warning btn-rounded m-b-10 m-l-5" data-toggle="modal" data-target="#RoomRegModal{{$op->id}}">Edit</button> </li>
                                            
                                            <button type="button" class="btn btn-danger btn-rounded m-b-10 m-l-5" data-toggle="modal" data-target="#RoomDelModal{{$op->id}}">Delete</button> </li>
                                            @include('pages.incubatee.roomreqinfodelmodal',['id'=>$op->id,'slot_list_id'=>$slot_list_id,'notify_id'=>$notify_id,'booked_id'=>$booked_id,'transaction_id'=> $transaction_id,'slot_id'=>$slot_id,'room'=>$op->room,'purpose'=>$op->purpose,'dt'=>$op->from_date])
                                              
                                            @include('pages.incubatee.roomreqinfoeditmodal',['id'=>$op->id,'slot_list_id'=>$slot_list_id,'notify_id'=>$notify_id,'booked_id'=>$booked_id,'transaction_id'=> $transaction_id,'slot_id'=>$slot_id,'room'=>$op->room,'purpose'=>$op->purpose,'dt'=>$op->from_date,'incubatee_email'=>$op->company_email])
                                            @endif     

                                                                             
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->





        </div>
</section>
@endsection
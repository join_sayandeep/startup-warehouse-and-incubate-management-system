  <!-- Jquery Core Js -->
  <script src="{{ asset('theme/plugins/jquery/jquery.min.js') }}"></script>

<!-- Bootstrap Core Js -->
<script src="{{ asset('theme/plugins/bootstrap/js/bootstrap.js') }}"></script>

<!-- Select Plugin Js -->
<script src="{{ asset('theme/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

<!-- Slimscroll Plugin Js -->
<script src="{{ asset('theme/plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

<!-- Waves Effect Plugin Js -->
<script src="{{ asset('theme/plugins/node-waves/waves.js') }}"></script>

<!-- Custom Js -->
<script src="{{ asset('theme/js/admin.js') }}"></script>

<!-- Demo Js -->
<script src="{{ asset('theme/js/demo.js') }}"></script>

<!-- Jquery DataTable Plugin Js -->
<script src="{{ asset('theme/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ asset('theme/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('theme/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('theme/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
<script src="{{ asset('theme/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
<script src="{{ asset('theme/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
<script src="{{ asset('theme/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
<script src="{{ asset('theme/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
<script src="{{ asset('theme/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
<script src="{{ asset('theme/js/pages/tables/jquery-datatable.js') }}"></script>
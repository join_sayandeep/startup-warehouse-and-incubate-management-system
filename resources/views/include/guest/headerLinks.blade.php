 <!-- Favicon-->
 <link rel="icon" href="{{ asset('theme/favicon.ico') }}" type="image/x-icon">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

<!-- Bootstrap Core Css -->
<link href="{{ asset('theme/plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

<!-- Waves Effect Css -->
<link href="{{ asset('theme/plugins/node-waves/waves.css') }}" rel="stylesheet" />

<!-- Animation Css -->
<link href="{{ asset('theme/plugins/animate-css/animate.css') }}" rel="stylesheet" />

<!-- Colorpicker Css -->
<link href="{{ asset('theme/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css') }}" rel="stylesheet" />

    <!-- Dropzone Css -->
<link href="{{ asset('theme/plugins/dropzone/dropzone.css') }}" rel="stylesheet">

   <!-- Multi Select Css -->
<link href="{{ asset('theme/plugins/multi-select/css/multi-select.css') }}" rel="stylesheet">

  <!-- Bootstrap Spinner Css -->
<link href="{{ asset('theme/plugins/jquery-spinner/css/bootstrap-spinner.css') }}" rel="stylesheet">

<!-- Bootstrap Tagsinput Css -->
<link href="{{ asset('theme/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">

 <!-- Bootstrap Select Css -->
<link href="{{ asset('theme/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />

   <!-- noUISlider Css -->
<link href="{{ asset('theme/plugins/nouislider/nouislider.min.css') }}" rel="stylesheet" />

<!-- Custom Css -->
<link href="{{ asset('theme/css/style.css') }} " rel="stylesheet">
<link href="{{ asset('theme/css/custom.css') }} " rel="stylesheet">

<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
<link href="{{ asset('theme/css/themes/all-themes.css') }}" rel="stylesheet" />

<!-- JQuery DataTable Css -->
<link href="{{ asset('theme/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
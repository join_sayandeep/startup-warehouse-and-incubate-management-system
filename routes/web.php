<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// only for back input
Route::get('/tal/records','GuestController@RecInput');
Route::post('/tal/records/save','GuestController@RecInputSave');
// Guest URL
Route::prefix('guest')->group(function () {
Route::get('/login','GuestController@ShowLogin');
Route::post('/login/check','GuestController@CheckLogin');
Route::get('/attendence','GuestController@ShowAttendence');
Route::post('/attendence/save','GuestController@saveAttendence');
Route::get('/attendence/show','GuestController@showAttendenceByDate');
Route::post('/attendence/checkout/status','GuestController@saveCheckoutTime');
});



// End Guest URL
Route::get('/admin/login', 'AdminController@loginFormShow');
Route::post('/admin/login/check', 'AdminController@loginCheck');

Route::group(['middleware' => 'adminstatus'], function () {

    Route::prefix('admin')->group(function () {
Route::get('/dashboard', 'AdminController@showDashboard');

Route::get('/create/operator/add', 'AdminController@addOperatorsForm');
Route::post('/operator/save','AdminController@saveOperators');
Route::get('/create/operator/show', 'AdminController@showOperatorsList');
Route::get('/operator/deactive/{email}/','AdminController@DeactiveOperator');
Route::get('/operator/active/{email}/','AdminController@ActiveOperator');

Route::get('/create/incubatee/add', 'AdminController@addIncubateeForm');
Route::post('/incubatee/save', 'AdminController@saveIncubatees');
Route::get('/create/incubatee/show', 'AdminController@showIncubateesList');
Route::get('/incubatee/deactive/{email}/','AdminController@DeactiveIncubatee');
Route::get('/incubatee/active/{email}/','AdminController@ActiveIncubatee');

Route::get('/booking/list','AdminController@reqRoomList');
Route::get('/booking/list/range','AdminController@reqRoomShow');
Route::post('/booking/list/range/show','AdminController@reqRoomByRangeDate');
Route::post('/booking/req','AdminController@statusChangeReqRoom');
Route::get('/booking/approved/{status}', 'AdminController@statusApprovedList');
Route::get('/booking/own/req','AdminController@bookaHall');
Route::post('/booking/save/check','AdminController@checkBookedHall');
Route::get('/booking/own/list','AdminController@showRoomBookingList');
Route::post('/bookedroom/delete','AdminController@deleteHallBookingInfo');
Route::post('/booking/save','AdminController@saveHallBookingInfo');
Route::post('/hallbook/update','AdminController@updateHallBookingInfo');
Route::post('/restore/req','AdminController@restoreHallBooking');
Route::get('/visitors/show', 'AdminController@showAttendence');
Route::post('/visitors/list', 'AdminController@showAttendenceByDate');

Route::get('/logout','AdminController@AdminLogout');
    });

});


//Incubatee

Route::get('/', 'IncubateeController@loginFormShow');
Route::post('/incubatee/login/check', 'IncubateeController@loginCheck');
Route::post('/incubatee/reset/pass/save','IncubateeController@resetFirstTimePassword');
Route::group(['middleware' => 'incubstatus'], function () {

    Route::prefix('incubatee')->group(function () {

Route::get('/profile', 'IncubateeController@profile');
Route::get('/profile/edit', 'IncubateeController@profileEdit');
Route::post('/profile/update','IncubateeController@profileUpdate');
Route::get('/profile/password','IncubateeController@editPassword');
Route::post('/profile/password/update','IncubateeController@updatePassword');
Route::get('/dashboard','IncubateeController@showDashboard');
// Route::get('/request/create','IncubateeController@createRequest');
// Route::post('/request/save/check','IncubateeController@checkRequest');
// Route::post('/request/save','IncubateeController@saveRequest');
// Route::get('/request/list','IncubateeController@showRequestList');
// Route::post('/roomreq/update','IncubateeController@updateRoomReqInfo');
// Route::post('/roomreq/delete','IncubateeController@deleteRoomReqInfo');

Route::get('/logout','IncubateeController@IncubateeLogout');

    });

});
 
// Operators
Route::get('/op/login', 'OperatorController@loginFormShow');
Route::post('/op/login/check', 'OperatorController@loginCheck');
Route::post('/op/password/reset/save','OperatorController@resetFirstTimePassword');
Route::group(['middleware' => 'opstatus'], function () {

    Route::prefix('op')->group(function () {
 
Route::get('/profile', 'OperatorController@profile');
Route::get('/profile/edit', 'OperatorController@profileEdit');
Route::post('/profile/save','OperatorController@profileSave');
Route::get('/password/update','OperatorController@passwordEdit');
Route::post('/password/save','OperatorController@passwordSave');
Route::get('/dashboard', 'OperatorController@showDashboard');
Route::get('/logout','OperatorController@OpLogout');
Route::get('/attendence','OperatorController@giveAttendence');
Route::post('/attendence/save','OperatorController@saveAttendence');
Route::get('/attendence/show','OperatorController@showAttendenceByDate');
Route::post('/attendence/checkout/status','OperatorController@saveCheckoutTime');
// Route::post('/attendence/show/dt','OperatorController@showAttendenceByDate');

// Logic for book a hall 
Route::get('/request/create','IncubateeController@createRequest');
Route::post('/request/save/check','IncubateeController@checkRequest');
Route::post('/request/save','IncubateeController@saveRequest');
Route::get('/request/list','IncubateeController@showRequestList');
Route::post('/roomreq/update','IncubateeController@updateRoomReqInfo');
Route::post('/roomreq/delete','IncubateeController@deleteRoomReqInfo');


    });

});